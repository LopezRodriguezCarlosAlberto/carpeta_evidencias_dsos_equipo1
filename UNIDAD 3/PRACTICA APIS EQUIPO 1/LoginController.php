<?php
 
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Models\AJAX\algo;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function blackList(Request $request){
    $id_actividad=$request->cod_postal;

    return response()->json($consulta=algo::select('*')->where('cod_postal','=',$id_actividad)
    ->take(1)->first());
    }

    public function insertar_black(Request $request){
        $id_unieco=$request->id_unieco;
        $nom_estab=$request->nom_estab;
        $cod_postal=$request->cod_postal;
        $id_actividad=$request->id_actividad;

        algo::create(['id_unieco'=>$id_unieco
                         ,'nom_estab'=>$nom_estab
                        ,'cod_postal'=>$cod_postal
                        ,'id_actividad'=>$id_actividad]);

        return response()->json(['mensaje'=>'registrado correctamente',]);

    }

    public function actualizar_black(Request $request){
        $id_unieco=$request->id_unieco;
        $raz_social=$request->raz_social;

        $editar =algo::select(
                            'id_unieco',
                            'raz_social')
                            ->where('id_unieco','=',$id_unieco)
                            ->update(['raz_social'=>$raz_social]);

       return response()->json(['mensaje'=>'editado correctamente',]);
    }

    public function eliminar_black(Request $request){
        $id_unieco=$request->id_unieco;
        $raz_social=$request->raz_social;

        $editar =algo::select('id_unieco','raz_social')
                            ->where('id_unieco','=',$id_unieco)
                            ->delete();

       return response()->json(['mensaje'=>'Eliminado correctamente',]);
    }
}