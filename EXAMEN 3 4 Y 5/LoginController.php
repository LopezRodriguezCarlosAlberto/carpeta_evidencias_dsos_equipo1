<?php
 
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Models\API\baseapi;
use Illuminate\Http\Request;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */


      //EXMANEN COMSUMO DE API CRUD

      public function obtieneApi($id){
        $respuesta = $this->peticion('GET',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/ver_nacimientos?id={$id}");
        $dato = json_decode($respuesta);

        return response()->json($dato);

    }

    public function consumir(){
        $client = new Client([
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded']
        ]);

        $response = $client->request('POST','http://dsos.jose/api/',
        ['form_params' => [
            ['nombre' => 'Roberto']

        ]]
        );

    }

    public function apiRegisgtra(){
        $respuesta = $this->peticion('POST',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/insertar_nacimiento",[
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'X-Requested-With' => 'XMLHttpRequest'
            ],
            'form_params' => [
                'edo_captura'=>'algo'
                ,'fecha_nac_madre'=>'1984-02-09'
                ,'edad_madre'=>'33'
                ,'estado_conyugal'=>'casada'
                ,'entidad_residencia_madre'=>'oaxaca'
                ,'numero_embarazos'=>'2'
                ,'hijos_nacidos_muertos'=>'0'
                ,'hijos_nacidos_vivos'=>'21'
                ,'hijos_sobrevivientes'=>'3'
                ,'fecha_nacimiento_nac_vivo'=>'1984-02-09'
                ,'hora_nacimiento_nac_vivo'=>'12:13'
                ,'sexo_nac_vivo'=>'d'
                ,'talla_nac_vivo'=>'2'
                ,'peso_nac_vivo'=>'23'
                ,'lugar_de_nacimiento'=>'d'
                ,'nombre_unidad_medica'=>'e'
                ,'entidad_nacimiento'=>'e'
                ,'certificado_por'=>'e'
                ,'entidad_certifico'=>'e'
                ,'fecha_certificacion'=>'1984-02-09'
            ]
        ]);
                $dato = json_decode($respuesta);

                return response()->json($dato);
    }


    public function apiActualizar($id)
{

$respuesta = $this->peticion('PUT',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/actualizar_nacimiento/{$id}",[

'headers' => [
    'Content=Type' => 'application/x-www-form-urlencoded',
    'X-Request-With' => 'XMLHttpRequest'
],
'forms_params'=> [
    'id' => $id,
    'edo_captura'=> 'prueba'
]
        ]);

        $datos = json_decode($respuesta);
        return response()->json($datos);

    }


    public function apiElimina($dato){
        $respuesta = $this->peticion('PUT',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/eliminar_nacimiento/{$dato}",
        [
            'headers' => [
               'Content-Type' => 'application/x-www-form-urlencoded',
               'X-Requested-With' => 'XMLHttpRequest'
            ],
            'form_params' =>[
               'edo_captura' => $dato
            ]
         ]
    );
        $datos = json_decode($respuesta);
        return response()->json($datos);
    }



    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
 
    public function blackList(Request $request){
        //buscar por codigo postal 
    $id_actividad=$request->id_unieco;

    return response()->json($consulta=baseapi::select('*')->where('id_unieco','=',$id_actividad)
    ->take(1)->first());
    }
 
    public function insertar_black(Request $request){
        $id_unieco=$request->id_unieco;
        $nom_estab=$request->nom_estab;
        $cod_postal=$request->cod_postal;
        $id_actividad=$request->id_actividad;
        /*$latitud=$request->latitud;
        $longitud=$request->longitud;
        $nomb_asent=$request->nomb_asent;
        $nom_e=$request->nom_e;
        $nom_vial=$request->nom_vial;
        $raz_social=$request->raz_social;
        $telefono=$request->telefono;
        $tipocencom=$request->tipocencom;*/

        baseapi::create(['id_unieco'=>$id_unieco
                         ,'nom_estab'=>$nom_estab
                        ,'cod_postal'=>$cod_postal
                        ,'id_actividad'=>$id_actividad
                       /* ,'latitud'=>$latitud
                        ,'longitud'=>$longitud
                        ,'nomb_asent'=>$nomb_asent
                        ,'nom_e'=>$nom_e
                        ,'nom_vial'=>$nom_vial
                        ,'raz_social'=>$raz_social
                        ,'telefono'=>$telefono
                        ,'tipocencom'=>$tipocencom*/]);

        return response()->json(['mensaje'=>'registrado correctamente',]);

    }

    public function actualizar_black(Request $request){
        $id_unieco=$request->id_unieco;
            $nom_estab=$request->nom_estab;
            $cod_postal=$request->cod_postal;
            $id_actividad=$request->id_actividad;
    
            $editar =baseapi::select('id_unieco','nom_estab','cod_postal','id_actividad')
                                ->where('id_unieco','=',$id_unieco)
                                ->update(['nom_estab'=>$nom_estab
                                ,'cod_postal'=>$cod_postal
                                ,'id_actividad'=>$id_actividad]);
    
           return response()->json(['mensaje'=>'editado correctamente',]);
    }


    public function eliminar_black(Request $request){
        $id_unieco=$request->id_unieco;

        $editar =baseapi::select('id_unieco','raz_social')
                            ->where('id_unieco','=',$id_unieco)
                            ->delete();
        //return "encontre".$id_unieco;
       return response()->json(['mensaje'=>'eliminado correctamente',]);
        }


       
}
