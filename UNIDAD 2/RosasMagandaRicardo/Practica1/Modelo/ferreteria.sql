-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-03-2019 a las 23:11:10
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dsos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ferreteria`
--

CREATE TABLE `ferreteria` (
  `id` int(11) NOT NULL,
  `razon` text NOT NULL,
  `giro` text NOT NULL,
  `domicilio` text NOT NULL,
  `rfc` text NOT NULL,
  `estado` text NOT NULL,
  `año` year(4) NOT NULL,
  `bandera` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ferreteria`
--

INSERT INTO `ferreteria` (`id`, `razon`, `giro`, `domicilio`, `rfc`, `estado`, `año`, `bandera`) VALUES
(2, 'Mantequilla Feliz', 'Pasteleria', '29 de diciembre', 'fkfkdofnn', 'Morelia', 1997, 1),
(3, 'Ricardo S.C', 'SOFTWARE', 'Avenia Siempre Viva', 'ROMRLFKF424KLS', 'Oaxaca', 2010, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ferreteria`
--
ALTER TABLE `ferreteria`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ferreteria`
--
ALTER TABLE `ferreteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
