<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table border=1>
        <tr>
            <td>ID</td>
            <td>RazonSocial</td>
            <td>Giro</td>
            <td>DomicilioFiscal</td>
            <td>RFC</td>
            <td>Estado</td>
            <td>AñoIngreso</td>
            <td>Bandera</td>
            <td colspan="3" align = "center"> Accion </td>
        </tr>

    @foreach($usuario as $c)
        <tr>
            <td>{{ $c->id}} </td>
            <td>{{ $c->razon}} </td>
            <td>{{ $c->giro}} </td>
            <td>{{ $c->domicilio}} </td>
            <td>{{ $c->rfc}} </td>
            <td>{{ $c->estado}} </td>
            <td>{{ $c->año}} </td>
            <td>{{ $c->bandera}} </td>
            <td><a href="editar_empresa/{{ $c->id}}">[Editar]</a></td>
            <td><a href="eliminar_ferre/{{ $c->id}}">[Eliminar]</a></td>
            <td><a href="eliminar_ferre_bandera/{{ $c->id}}">[Eliminar_Bandera]</a></td>
        </tr>
    @endforeach
    </table>

      <p><a href="agregar">[Agregar Empresa]</a></p>
  </body>
  </html>