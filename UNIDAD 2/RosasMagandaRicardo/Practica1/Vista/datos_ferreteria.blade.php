<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        {!!Form::open(array('url'=>'insertar_ferreteria','method'=>'POST','autocomplete'=>'off'))!!}
        
        {!!Form::label('Razon Social: ') !!}
        {!!Form::text('razon',null)!!}
        <br>
        {!!Form::label('Giro: ') !!}
        {!!Form::text('giro',null)!!}
        <br>
        {!!Form::label('Domicilio Fiscal: ') !!}
        {!!Form::text('domicilio',null)!!}
        <br>
        {!!Form::label('RFC: ') !!}
        {!!Form::text('rfc',null)!!}
        <br>
        {!!Form::label('Estado: ')!!}
        {!!Form::text('estado',null)!!}
        <br>
        {!!Form::label('Año: ') !!}
        {!!Form::text('año',null)!!}
        <br> <br>

        {!!Form::submit('Registrar',['razon'=>'grabar','giro'=>'grabar','domicilio'=>'grabar',
        'rfc'=>'grabar','estado'=>'grabar','año'=>'grabar','content'=>'<span>Registrar<span>'])!!}

        {!!Form::close()!!}
    </body>
</html>