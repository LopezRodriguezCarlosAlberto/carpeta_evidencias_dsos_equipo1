<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
    {!!Form::open(array('url'=>'borrar_por_bandera/'.$uno->id,'method'=>'PUT','autocomplete'=>'off'))!!}
        
        {!!Form::label('ID: ') !!}
        {!!Form::text('id',$uno->id)!!}
        <br>
        {!!Form::label('Razon Social: ') !!}
        {!!Form::text('razon',$uno->razon)!!}
        <br>
        {!!Form::label('Giro: ') !!}
        {!!Form::text('giro',$uno->giro)!!}
        <br>
        {!!Form::label('Domicilio Fiscal: ') !!}
        {!!Form::text('domicilio',$uno->domicilio)!!}
        <br>
        {!!Form::label('RFC: ') !!}
        {!!Form::text('rfc',$uno->rfc)!!}
        <br>
        {!!Form::label('Estado: ')!!}
        {!!Form::text('estado',$uno->estado)!!}
        <br>
        {!!Form::label('Año: ') !!}
        {!!Form::text('año',$uno->año)!!}
        <br> <br>

        {!!Form::submit('Eliminar',['id'=>'act','razon'=>'act','giro'=>'act',
            'domicilio'=>'act','rfc'=>'act','estado'=>'act','año'=>'act',
            'content'=>'<span>Eliminar<span>'])!!}

        {!!Form::close()!!}
    </body>
</html>