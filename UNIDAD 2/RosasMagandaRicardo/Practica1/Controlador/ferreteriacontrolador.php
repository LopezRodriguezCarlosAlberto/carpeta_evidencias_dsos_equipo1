<?php

namespace app\Http\Controllers\Dsos_controller;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\PW\ferreteriamodelo; //PARA LA BASE DE DATOS

class ferreteriacontrolador extends Controller
	{
        public function ver_tabla(){
            $vertodo = ferreteriamodelo::where('bandera','1')->get();
            return view('DSOS/ver_tabla_ferreteria')->with('usuario',$vertodo);
        }

        public function formulario_ferreteria(){
			return view('DSOS/datos_ferreteria');
        }

        public function insert_ferre(Request $request){
            $razon = $request->input('razon');
			$giro = $request->input('giro');
			$domicilio = $request->input('domicilio');
			$rfc = $request->input('rfc');
			$estado = $request->input('estado');
            $año = $request->input('año');

            ferreteriamodelo::create(['razon'=>$razon,'giro'=>$giro,'domicilio'=>$domicilio,
            'rfc'=>$rfc,'estado'=>$estado,'año'=>$año]);

			return redirect()->to('ferreteria');
        }

        public function edit_ferre($id){
            $uno = ferreteriamodelo::
            where('id',$id)->take(1)->first();

            return view ('DSOS/actualizar_ferreteria')->with ('uno', $uno);
        }

        public function actualizar_datos(Request $data,$id){
            $editar = ferreteriamodelo::find($id);

            $editar->id = $data->id;
            $editar->razon = $data->razon;
            $editar->giro = $data->giro;
            $editar->domicilio = $data->domicilio;
            $editar->rfc = $data->rfc;
            $editar->estado = $data->estado;
            $editar->año = $data->año;

            $editar->save();

            return redirect()->to('ferreteria');
        }

        public function edit_datos_eliminar($id){
            $uno = ferreteriamodelo::where('id',$id)->take(1)->first();
            return view ('DSOS/elimina_def')->with ('uno', $uno);
        }

        public function edit_eliminar($id){
            $editar = ferreteriamodelo::find($id);
            $editar->delete();
            return redirect()->to('ferreteria');
        }

        public function editar_bandera($id){
            $uno = ferreteriamodelo::
            where('id',$id)->where('bandera',1)->take(1)->first();
                            //where('bandera','=','1')
            return view ('DSOS/ver_ferre_bandera')->with ('uno', $uno);
        }

        public function bandera_eliminado($id)
        {
            $editar_b = ferreteriamodelo::find($id);
            $editar_b->bandera = '0';
            $editar_b->save();

            return redirect()->to('ferreteria');
        }
        
        
    }