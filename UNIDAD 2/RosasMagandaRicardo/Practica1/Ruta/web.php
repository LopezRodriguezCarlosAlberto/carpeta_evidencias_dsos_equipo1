<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/hola','Dsos_controller\micontrolador@index');

Route::get('/inicio','Dsos_controller\micontrolador@indexinicio');

//PARA LA BASE DE DATOS
Route::get('ver_datos','Dsos_controller\micontrolador@ver_datos');

//PARA LA BASE DE DATOS CON BLADE
Route::get('visualizar','Dsos_controller\micontrolador@ver_nuevosdatos');

//PARA LA BASE DE DATOS CON BLADE practica 2
Route::get('practica2','Dsos_controller\practica2controlador@ver_nuevosdatos2');

//PARA REGISTRAR EN UN FORMUALARIO PRACTICA 2
Route::get('formulario','Dsos_controller\practica2controlador@ver_formulario');
Route::post('insertar','Dsos_controller\practica2controlador@insertar');
Route::get('formulariolleno','Dsos_controller\practica2controlador@ver_nuevosdatos');

//PARA REGISTRAR EN UN FORMUALARIO PRACTICA 3
Route::get('formulario2','Dsos_controller\practica3controlador@ver_formulario');
Route::post('insertar2','Dsos_controller\practica3controlador@insertar');
Route::get('formulariolleno2','Dsos_controller\practica3controlador@ver_nuevosdatos');

//PARA ACTUALIZAR DATOS PRACTICA 4
Route::get('visualizar4','Dsos_controller\practica4controlador@ver_datos'); //FORMULARIO CON EL PRIMER REGISTRO
Route::get('formulario','Dsos_Controller\practica4controlador@ver_formulario'); //FORMULARIO PARA AGREGAR UN NUEVO REGISTRO
Route::post('insertar4','Dsos_Controller\practica4controlador@insertar'); //BOTON DE INSERTAR
Route::get('actualizar/{id}','Dsos_controller\practica4controlador@edit_datos'); //CONSULTAR DEACUERDO AL ID
Route::put('actualizar_datos/{id}','Dsos_controller\practica4controlador@actualizar_datos'); //BOTON DE ACTUALIZAR CAMBIANDO DATOS

//PRACTICA 5 ENVIO MATERIAS-ALUMNOS
Route::get('insertar_practica5','Dsos_controller\practica5controlador@ver_formulario');//FORMULARIO PARA AGREGAR UN NUEVO REGISTRO
Route::post('insertar5','Dsos_Controller\practica5controlador@insertar'); //BOTON DE INSERTAR
Route::get('editar_practica5/{id}','Dsos_controller\practica5controlador@edit_datos'); //CONSULTAR DEACUERDO AL ID
Route::put('actualizar_datos/{id}','Dsos_controller\practica5controlador@actualizar_datos'); //BOTON DE ACTUALIZAR CAMBIANDO DATOS

//INICIO DE LA SEGUNDA UNIDAD

//PRACTICA 6 ELIMINAR REGISTROS
Route::get('eliminar_datos/{id}','Dsos_controller\practica6controlador@edit_datos_eliminar'); 
Route::put('actualizar_datos6/{id}','Dsos_controller\practica6controlador@edit_eliminar'); 

//PRACTICA 7 ELIMINAR POR BANDERA
Route::get('eliminar_bandera/{id}','Dsos_controller\practica7controlador@editar_bandera'); 
Route::put('bandera_eliminar/{id}','Dsos_controller\practica7controlador@bandera_eliminado'); 

//PRACTICA 8 VER_TABLA
Route::get('tabla','Dsos_controller\tablacontrolador@ver_tabla');


//PRACTICA 5 CRUD FERRETERIA
Route::get('ferreteria','Dsos_controller\ferreteriacontrolador@ver_tabla');
//INSERTAR
Route::get('agregar','Dsos_controller\ferreteriacontrolador@formulario_ferreteria');
Route::post('insertar_ferreteria','Dsos_controller\ferreteriacontrolador@insert_ferre');
//EDITAR
Route::get('editar_empresa/{id}','Dsos_controller\ferreteriacontrolador@edit_ferre');
Route::put('update_ferre/{id}','Dsos_controller\ferreteriacontrolador@actualizar_datos');
//ELIMINAR
    Route::get('eliminar_ferre/{id}','Dsos_controller\ferreteriacontrolador@edit_datos_eliminar'); 
    Route::put('borrar_def/{id}','Dsos_controller\ferreteriacontrolador@edit_eliminar'); 
//ELIMINAR POR BANDERA
    Route::get('eliminar_ferre_bandera/{id}','Dsos_controller\ferreteriacontrolador@editar_bandera'); 
    Route::put('borrar_por_bandera/{id}','Dsos_controller\ferreteriacontrolador@bandera_eliminado'); 


Route::get('/', function () {
    return view('welcome');
}); 