<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// RUTAS PARA AJAX
Route::get('lista_alumnos/{genero}','AJAx\AjaxController@listado_alumnos');
Route::get('ajax','AJAx\AjaxController@formu');

Route::get('/', function () {
    return view('welcome');
}); 