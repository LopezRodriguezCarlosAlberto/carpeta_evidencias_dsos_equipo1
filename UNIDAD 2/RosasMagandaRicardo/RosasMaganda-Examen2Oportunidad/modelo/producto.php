<?php
//PARA LA BASE DE DATOS
namespace App\Models\AJAX;

use Illuminate\Database\Eloquent\Model;

class producto extends Model{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'producto';
    //aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    //aqui van los elementos a mostrarse en cuestion
    protected $fillable = [
        'id','producto','precio','cantidad','descuento','precio_final'
    ];
}
?>