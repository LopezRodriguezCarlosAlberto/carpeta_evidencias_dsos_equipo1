<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAX\Sexo;
use App\Models\AJAX\Alumnos;
use App\Models\AJAX\alumno;
use App\Models\AJAX\Materias;
use App\Models\AJAX\Semestre;
use App\Models\AJAX\TiendaModel;
use App\Models\AJAX\ProductoModel;
use App\Models\AJAX\relacion;
use App\Models\AJAX\producto;
use App\Models\AJAX\nuevo;


class AjaxController extends Controller
{
    /////////////////////-EJEMPLO1-//////////////////////
    public function formu()
    {
        $enviar = Sexo::pluck('sexo','id');
        return view('AJAX/ejemplo1')->with('sex',$enviar);
    }

    public function listado_alumnos($genero)
    {
        $al = Alumnos::select('id','nombre_completo','sexo')
        ->where('sexo',$genero)
        ->get();
          return $al;
    }
    //////////////////////-EJEMPLO2-/////////////////////
    public function listado_semestre($materias)
    {
        $al = Materias::select('id','materia','semestre')
        ->where('semestre',$materias)
        ->get();
          return $al;
    }

    public function formuSemestre(){
        $enviar = semestre::pluck('semestre','id');
        return view('AJAX/ejemplo2')->with('semestre',$enviar);
    }

    public function insertar(Request $request)
    {
        //$id = 0;
        $nombre_alumno = $request->input('nombre_alumno');
        $numero_control = $request->input('numero_control');
        $semestre = $request->input('idsemestre');
        $materia = $request->input('idmateria'); 

      alumno::create([/*'id' => 0,*/'nombre_alumno' => $nombre_alumno,'numero_control' => $numero_control,
      'semestre' => $semestre,'materia' => $materia]); 
      return redirect()->to('ajaxSemestre');
    }
    //////////////////////-EJEMPLO3-/////////////////////
    public function listado_producto($materias)
    {
        $al = nuevo::select('id','nombre','preciou')
        ->where('id',$materias)
        ->get();
          return $al;
    }

    public function formuTienda2(){
        return view('AJAX/insertar_empresa');
    }

    public function formuTienda3(){
        return view('AJAX/insertar_productos');
    }

    public function formuTienda4(){
        $enviar = ProductoModel::pluck('nombre','id');
        return view('AJAX/tiendita')->with('nombre',$enviar);
    }

    public function insertarEmpresa(Request $request){
        $rfc = $request->input('rfc');
        $razon = $request->input('razon');
        $direccion = $request->input('direccion');
        $apoderado = $request->input('apoderado');
        $telefono = $request->input('telefono');

        TiendaModel::create(['rfc'=>$rfc,'razon'=>$razon,'direccion'=>$direccion,
        'apoderado'=>$apoderado,'telefono'=>$telefono]);

        return redirect()->to('ajaxEmpresa');
    }

    public function insertarProducto(Request $request){
        $nombre = $request->input('nombre');
        $tipo = $request->input('tipo');
        $proveedor = $request->input('proveedor');
        $preciou = $request->input('preciou');
        $preciov = $request->input('preciov');

        ProductoModel::create(['nombre'=>$nombre,'tipo'=>$tipo,'proveedor'=>$proveedor,
        'preciou'=>$preciou,'preciov'=>$preciov]);

        return redirect()->to('ajaxProducto');
    }

    public function edit_empre($id){
            $uno = TiendaModel::
            where('id',$id)->where('bandera',1)->take(1)->first();

            return view ('AJAX/actualizar_datos5')->with ('uno', $uno);
    }

    public function actualizar_datos(Request $data,$id){
            $editar = TiendaModel::find($id);
            $editar->rfc = $data->rfc;
            $editar->razon = $data->razon;
            $editar->direccion = $data->direccion;
            $editar->apoderado = $data->apoderado;
            $editar->telefono = $data->telefono;
            $editar->save();

            return redirect()->to('ajaxEmpresa');
    }

    public function edit_datos_eliminar($id){
        $uno = TiendaModel::
        where('id',$id)->take(1)->first();
        return view ('AJAX/ver_tienda_bandera')->with ('uno', $uno);
    }

    public function bandera_eliminado($id){
        $editar_b = TiendaModel::find($id);
        $editar_b->bandera = '0';
        $editar_b->save();

        return redirect()->to('ajaxEmpresa');
    }

    public function formuProductos(){
        return view('AJAX/formularioProductos');
    }

    public function formuProductos2($id){
        $uno = producto::where('id',$id)->take(1)->first();
        return view('AJAX/formularioProductos2')->with('uno', $uno);
    }

    public function insertarNuevoProducto(Request $request){
        $nombre = $request->input('producto');
        $precio = $request->input('precio');
        $cantidad = $request->input('input_cantidad');
        $descuento = $request->input('descuento');
        $precio_final = $request->input('total');

        producto::create(['producto'=>$nombre,'precio'=>$precio,'cantidad'=>$cantidad,
        'descuento'=>$descuento,'precio_final'=>$precio_final]);

        return redirect()->to('examen');
    }

    public function edit_eliminar($id){
        $editar = producto::find($id);
        $editar->delete();
        return redirect()->to('examen');
        
    }
}
?>