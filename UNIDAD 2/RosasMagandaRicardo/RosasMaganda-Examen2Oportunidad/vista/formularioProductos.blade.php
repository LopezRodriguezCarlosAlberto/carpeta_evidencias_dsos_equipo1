<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
    <script src="js/jquery.min.js"></script>
    <script src="js/cargar_productos.js"></script>
<body>
    {!!Form::open(array('url'=>'insertarNuevoProducto','method'=>'POST','autocomplete'=>'off'))!!}

    {!!Form::label('Producto: ') !!}
    {!!Form::text('producto',null)!!}
    <br>
    {!!Form::label('Precio Unitario: ') !!}
    <input type="text" name="precio" id="precio">
    <br>  
    {!!Form::label('Cantidad: ')!!}
        <select name="input_cantidad" id="input_cantidad">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        </select>
    <br>
    {!!Form::label('Descuento: ') !!}
    <input type="text" name="descuento" id="descuento">
    <br>
    {!!Form::label('Costo Total:') !!}
        <input type="text" name="total" id="total">

    {!!Form::submit('Registrar',['producto'=>'grabar','precio'=>'grabar',
            'input_cantidad'=>'grabar','descuento'=>'grabar','total'=>'grabar',
            'content'=>'<span>Registrar<span>'])!!}

    {!!Form::close()!!}
<br>
</body>
</html>