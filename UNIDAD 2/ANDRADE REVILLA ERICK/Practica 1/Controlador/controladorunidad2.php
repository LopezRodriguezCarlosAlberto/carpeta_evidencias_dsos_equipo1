<?php
    namespace app\Http\Controllers\PW_controler;
    
    use Illuminate\Http\Request;

    use App\Http\Controllers\Controller;

    use App\Models\PW\modelou2;

    class controladorunidad2 extends Controller
    {
        public function verTabla()
        {
            $vertodo = modelou2::
            where('bandera','1')->get();

            return view('PW/unidad2')
            ->with('usuario',$vertodo);
        }

        public function editu2($id)
        {
            $uno = modelou2::where('id',$id)->take(1)->first();//primera opcion para editar un campo

            //$dos = modelop3::find($id);//segunda opcion para modificar un dato

            return view('PW\modificaru2')//modificarp3 es la vista para ver los datos solicitados
                ->with('uno', $uno);
        }

        public function actualizaru2(Request $data, $id)
        {
            $editar = modelou2::find($id);

            $editar->id = $data->id;
            $editar->razonSocial = $data->razonSocial;
            $editar->giro = $data->giro;
            $editar->domicilioFiscal = $data->domicilioFiscal;
            $editar->rfc = $data->rfc;
            $editar->estado = $data->estado;
            $editar->añoIngreso = $data->añoIngreso;

            $editar->save();

            return redirect()->to('tablau2');//esta ruta redirige hacia la vista en la qu muestra el primer registro de la tabla
            //return redirect()-to('tabla');
        }

        public function eliminar($id)
        {
            $uno = modelou2::where('id',$id)->take(1)->first();//primera opcion para editar un campo

            //$dos = modelop3::find($id);//segunda opcion para modificar un dato

            return view('PW\eliminaru2')//modificarp3 es la vista para ver los datos solicitados
                ->with('uno', $uno);
        }

        public function eliu2($id)
        {
            $editar = modelou2::find($id);

            //$editar->delete();
            $editar->bandera = 0;

            $editar->save();

            return redirect()->to('tablau2');//esta vista muestra la vista del ultimo  registro
        }

        public function verforu2() 
       {
           return view ("PW\insertaru2");
       }

       public function insertardatosu2(Request $data)
        {
            $id = $data->input('id');
            $razonSocial = $data->input('razonSocial');
            $giro = $data->input('giro');
            $domicilioFiscal = $data->input('domicilioFiscal');
            $rfc = $data->input('rfc');
            $estado = $data->input('estado');
            $añoIngreso = $data->input('añoIngreso');

            modelou2::create(['id'=>$id,'razonSocial'=>$razonSocial,'giro'=>$giro,'domicilioFiscal'=>$domicilioFiscal,'rfc'=>$rfc,'estado'=>$estado,'añoIngreso'=>$añoIngreso]);
            return redirect()->to('tablau2');//a donde debe de ir despues de realizar la insercion
        }//(redirigir hacia la vista que muestra el primer registro)

        public function eliminapu2($id)
        {
            $uno = modelou2::where('id',$id)->take(1)->first();//primera opcion para editar un campo

            //$dos = modelop3::find($id);//segunda opcion para modificar un dato

            return view('PW\eliminapu2')//modificarp3 es la vista para ver los datos solicitados
                ->with('uno', $uno);
        }

        public function eliminaper($id)
        {
            $editar = modelou2::find($id);

            $editar->delete();//delete los elima permanentemente

            return redirect()->to('tablau2');//(verdatosp4 es una ruta para mostrar los datos en general de la tabla)
        }
    }
?>