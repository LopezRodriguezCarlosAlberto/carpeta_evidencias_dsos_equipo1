<?PHP

namespace App\Models\PW;

use Illuminate\Database\Eloquent\Model;

class modelou2 extends Model
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'unidad2';
    //aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    //aqui los elementos a mostrarse en la tabla e cuestion
    protected $fillable = [
        'id','razonSocial','giro','domicilioFiscal','rfc','estado','añoIngreso'
    ];
}