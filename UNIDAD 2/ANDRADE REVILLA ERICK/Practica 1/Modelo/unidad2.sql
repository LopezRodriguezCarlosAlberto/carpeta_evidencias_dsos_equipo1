-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-03-2019 a las 21:09:12
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practicas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad2`
--

CREATE TABLE `unidad2` (
  `id` int(11) NOT NULL,
  `razonSocial` text NOT NULL,
  `giro` text NOT NULL,
  `domicilioFiscal` text NOT NULL,
  `rfc` text NOT NULL,
  `estado` text NOT NULL,
  `añoIngreso` int(11) NOT NULL,
  `bandera` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidad2`
--

INSERT INTO `unidad2` (`id`, `razonSocial`, `giro`, `domicilioFiscal`, `rfc`, `estado`, `añoIngreso`, `bandera`) VALUES
(1, 'aaaaaaa', 'kskssk', 'kkdkdkd', 'kdkdk', 'dkdkdk', 111, 0),
(2, 'ksks', 'pspsp', 'spsp', 'ññsñs', 'ñsñña', 123, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `unidad2`
--
ALTER TABLE `unidad2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `unidad2`
--
ALTER TABLE `unidad2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
