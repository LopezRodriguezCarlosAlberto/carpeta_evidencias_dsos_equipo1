<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Unidad2</title>
</head>
<body>

    <table border=1>
        <tr>
            <td>Id</td>
            <td>Razon Social</td>
            <td>Giro</td>
            <td>DomicilioFiscal</td>
            <td>RFC</td>
            <td>Estado</td>
            <td>AñoIngreso</td>
            <td colspan="3" align="center">Accion</td>
        </tr>

        @foreach($usuario as $c)
            <tr>
                <td>{{$c->id}}</td>
                <td>{{$c->razonSocial}}</td>
                <td>{{$c->rfc}}</td>
                <td>{{$c->giro}}</td>
                <td>{{$c->domicilioFiscal}}</td>
                <td>{{$c->estado}}</td>
                <td>{{$c->añoIngreso}}</td>
                <td><a href="actualizaru2/{{$c->id}}">[Editar]</a></td>
                <td><a href="eliminau2/{{$c->id}}">[Eliminar]</a></td>
                <td><a href="eliminaperu2/{{$c->id}}">[EliminarPermanente]</a></td>
            <tr>
        @endforeach
    </table>

    <a href="insertaru2/">Insertar</a>
</body>
</html>