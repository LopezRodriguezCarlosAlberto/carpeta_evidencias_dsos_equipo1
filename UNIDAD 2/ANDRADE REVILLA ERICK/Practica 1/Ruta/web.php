<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('hola','PW_controler\Mi_controlador@index');

Route::get('sesion','PW_controler\Mi_controlador@isesion');

//ejercicio mostrar datos de tabla
Route::get('datos','PW_controler\Mi_controlador@verDatos');//muestra el prmer registro

//Practica 2
Route::get('practica2','PW_controler\nuevocontrolador@datos');//muestra el primer registro de la tabla
Route::get('formulario','PW_controler\nuevocontrolador@verFormulario');//muestra el formulario para registrar
Route::post('insertar','PW_controler\nuevocontrolador@insertar');//es llamada por la vista insertarDatos(inserta en la tabla)

//PRACTICA 3 inserta y luego muestra el primer registro
Route::get('verdatosp3','PW_controler\controladorp3@datos');//para visualizar el primer registro que se insert
Route::get('formulariop3','PW_controler\controladorp3@verFormulariop3');//muestra el formulario
Route::post('insertardatosp3','PW_controler\controladorp3@insertardatosp3');//es llamada por la vista insertarp3 (inserta en la tabla)

//modificar/actualizar un registro
Route::get('actualizar/{id}','PW_controler\controladorp3@editDatos');//muestra el registro seleccionado
Route::put('actualizarDatos/{id}','PW_controler\controladorp3@actualizarDatos');//ejecuta la funcion para modificar el registro

//practica 4
Route::get('verdatosp4','PW_controler\controladorp4@verDatos');//ver datos de la tabla practica 4
//insertar
Route::get('formulariop4','PW_controler\controladorp4@verFormulariop4');//muestra el formulario
Route::post('insertardatosp4','PW_controler\controladorp4@insertardatosp4');//es llamada por la vista insertarp3 (inserta en la tabla)
//actualizar/modificar practica 4
Route::get('actualizarp4/{id}','PW_controler\controladorp4@editDatosp4');//muestra el registro seleccionado
Route::put('actualizarDatosp4/{id}','PW_controler\controladorp4@actualizarDatosp4');//ejecuta la funcion para modificar el registro
//eliminar datos de forma permanente p5
Route::get('eliminarDatosp5/{id}','PW_controler\controladorp3@eliminaDatosp5');//ejecuta el metodo que muestra el registro a ser eliminado
Route::put('actualizarDatosp5/{id}','PW_controler\controladorp3@actualizarDatosp5');//ejecuta el metodo para eliminar el registro de forma permanente
//eliminar datos con banderas (no de forma permanente) p6
Route::get('eliminarDatosp6/{id}','PW_controler\controladorp3@eliminaDatosp6');//ejecuta el metodo que muestra el registro a ser eliminado
Route::put('actualizarDatosp6/{id}','PW_controler\controladorp3@actualizarDatosp6');//ejecuta el metodo para desactivar al registro
//ruta para mostrar la tablade lapractica numero 7
Route::get('tabla','PW_controler\controladorp3@verTabla');
//Unidad2
Route::get('tablau2','PW_controler\controladorunidad2@verTabla');

Route::get('actualizaru2/{id}','PW_controler\controladorunidad2@editu2');
Route::put('actualizaU2/{id}','PW_controler\controladorunidad2@actualizaru2');

Route::get('eliminau2/{id}','PW_controler\controladorunidad2@eliminar');
Route::put('eliminaru2/{id}','PW_controler\controladorunidad2@eliu2');

Route::get('insertaru2','PW_controler\controladorunidad2@verforu2');
Route::post('insertandou2','PW_controler\controladorunidad2@insertardatosu2');

Route::get('eliminaperu2/{id}','PW_controler\controladorunidad2@eliminapu2');//ejecuta el metodo que muestra el registro a ser eliminado
Route::put('eliminaper/{id}','PW_controler\controladorunidad2@eliminaper');
?>