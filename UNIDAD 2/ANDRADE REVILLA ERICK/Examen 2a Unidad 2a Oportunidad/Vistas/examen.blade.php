<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<script src="js/jquery.min.js"></script>
<script src="js/examen.js"></script>
<body>
        <div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="panel panel-default">
    {!!Form::open(array('url'=>'iexamen','method'=>'POST','autocomplete'=>'off'))!!}
    <div class="form-group">
	    {!! Form::label('PRODUCTO: ') !!}
		{!!Form::text('producto',null,['id'=>'producto','class'=>'form-control','placeholder'=>'Producto'])!!}
	</div>
	<div class="form-group">
		{!! Form::label('PRECIO: ') !!}
    	{!!Form::text('precio',null,['id'=>'precio','class'=>'form-control','placeholder'=>'precio'])!!}
	</div>
	<div class="form-group">
		{!! Form::label('CANTIDAD: ') !!}
		{!!Form::text('cantidad',null,['id'=>'cantidad','class'=>'form-control','placeholder'=>'cantidad'])!!}
	</div>
	<div class="form-group">
		{!! Form::label('DESCUENTO: ') !!}
		{!!Form::text('descuento',null,['id'=>'descuento','class'=>'form-control','placeholder'=>'descuento'])!!}
	</div>
	<div class="form-group">
		{!! Form::label('PRECIO FINAL: ') !!}
		{!!Form::text('preciofinal',null,['id'=>'preciofinal','class'=>'form-control','placeholder'=>'preciofinal'])!!}
    </div>
    {!!Form::submit('Registrar',['producto'=>'grabar','precio'=>'grabar','cantidad'=>'grabar',
								'descuento'=>'grabar','preciofinal'=>'grabar',
								'content'=>'<span>REGISTRAR</span>'])!!}
    {!!Form::close()!!}
    
</div>
</div>
</div>
</div>
</body>
</html>