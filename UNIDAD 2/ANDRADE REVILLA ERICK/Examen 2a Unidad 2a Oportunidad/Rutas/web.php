<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

////////CONTROL DE VISTAS/////

Route::get('registroChecador','m_controles\control_vistas@verChecador');
Route::get('registroChofer','m_controles\control_vistas@verChofer');
Route::get('registroConcesion','m_controles\control_vistas@verConcesion');
Route::get('registroDirectivo','m_controles\control_vistas@verDirectivo');
Route::get('registroEntradas','m_controles\control_vistas@verEntradas');
Route::get('registroMecanica','m_controles\control_vistas@verRevision');
Route::get('registroSeguro','m_controles\control_vistas@verSeguro');
Route::get('registroSocio','m_controles\control_vistas@verSocio');
Route::get('registroAuto','m_controles\control_vistas@verVehiculo');

Route::get('examen','m_controles\control_vistas@verExamen');
/////////////////////////////



///////RUTAS PARA INSERTAR//////

Route::post('ichecador','m_controles\control_regis@insertachecador');
Route::post('ivehiculo','m_controles\control_regis@insertavehiculo');
Route::post('isocio','m_controles\control_regis@insertasocio');
Route::post('iconcesion','m_controles\control_regis@insertaconcesion');
Route::post('idirectivo','m_controles\control_regis@insertadirectivo');
Route::post('iseguro','m_controles\control_regis@insertaseguro');
Route::post('ientrada','m_controles\control_regis@insertaentrada');//(en la BD es la tabla registro)
Route::post('irevision','m_controles\control_regis@insertarevision');
Route::post('ichofer','m_controles\control_regis@insertachofer');

Route::post('iexamen','m_controles\control_regis@insertaexamen');

Route::get('eexamen/{id}','m_controles\control_regis@eliminaexamen');
Route::put('actuexa/{id}','m_controles\control_regis@actualizar');//ejecuta el metodo para eliminar el registro de forma permanente
///////////////////////////////