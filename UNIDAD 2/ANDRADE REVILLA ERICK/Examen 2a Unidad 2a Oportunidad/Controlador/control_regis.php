<?php

namespace App\Http\Controllers\m_controles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\m_modelos\mchecador;
use App\Models\m_modelos\mvehiculo;
use App\Models\m_modelos\msocio;
use App\Models\m_modelos\mconcesion;
use App\Models\m_modelos\mdirectivo;
use App\Models\m_modelos\mseguro;
use App\Models\m_modelos\mregistro;
use App\Models\m_modelos\mchofer;
use App\Models\m_modelos\mexamen;

class control_regis extends Controller
{
    public function insertachecador(Request $request)
    {
        $nombre = $request->input('nombre');
        $ap_p = $request->input('ap_p');
        $ap_m = $request->input('ap_m');
        $curp = $request->input('curp');
        $domicilio = $request->input('domicilio');
        $telefono = $request->input('telefono');
        $correo_e = $request->input('correo_e');

        mchecador::create(['nombre'=>$nombre,'ap_p'=>$ap_p,'ap_m'=>$ap_m,
        'curp'=>$curp,'domicilio'=>$domicilio,'telefono'=>$telefono,'correo_e'=>$correo_e]);
        return redirect()->to('registroChecador');
    }

    public function insertavehiculo(Request $request)
    {
        $num_sitio = $request->input('num_sitio');
        $num_placas = $request->input('num_placas');
        $num_unico = $request->input('num_unico');
        $tipo = $request->input('tipo');
        $modelo = $request->input('modelo');
        $num_motor = $request->input('num_motor');
        $num_serie = $request->input('num_serie');
        $color = $request->input('color');
        $capacidad = $request->input('capacidad');
        $foto_v = $request->input('foto_v');
        $observaciones = $request->input('observaciones');

        mvehiculo::create(['num_sitio'=>$num_sitio,'num_placas'=>$num_placas,'num_unico'=>$num_unico,
        'tipo'=>$tipo,'modelo'=>$modelo,'num_motor'=>$num_motor,'num_serie'=>$num_serie,'color'=>$color
        ,'capacidad'=>$capacidad,'foto_v'=>$foto_v,'observaciones'=>$observaciones]);
        return redirect()->to('registroAuto');
    }

    public function insertasocio(Request $request)
    {
        $nombre = $request->input('nombre');
        $ap_p = $request->input('ap_p');
        $ap_m = $request->input('ap_m');
        $fecha_n = $request->input('fecha_n');
        $sexo = $request->input('sexo');
        $curp = $request->input('curp');
        $rfc = $request->input('rfc');
        $domicilio = $request->input('domicilio');
        $ocupacion = $request->input('ocupacion');
        $telefono = $request->input('telefono');
        $codigo_p = $request->input('codigo_p');
        $correo_e = $request->input('correo_e');

        msocio::create(['nombre'=>$nombre,'ap_p'=>$ap_p,'ap_m'=>$ap_m,'fecha_n'=>$fecha_n,
        'sexo'=>$sexo,'curp'=>$curp,'rfc'=>$rfc,'domicilio'=>$domicilio,'ocupacion'=>$ocupacion,
        'telefono'=>$telefono,'codigo_p'=>$codigo_p,'correo_e'=>$correo_e]);
        return redirect()->to('registroSocio');
    }

    public function insertaconcesion(Request $request)
    {
        $num_unico = $request->input('num_unico');
        $modalidad = $request->input('modalidad');
        $fecha_ap = $request->input('fecha_ap');
        $fecha_v = $request->input('fecha_v');
        $clave_distrito = $request->input('clave_distrito');
        $localidad = $request->input('localidad');
        $idSocio = $request->input('idSocio');
        $idVehiculo = $request->input('idVehiculo');

        mconcesion::create(['num_unico'=>$num_unico,'modalidad'=>$modalidad,'fecha_ap'=>$fecha_ap,'fecha_v'=>$fecha_v,
        'clave_distrito'=>$clave_distrito,'localidad'=>$localidad,'idSocio'=>$idSocio,'idVehiculo'=>$idVehiculo]);
        return redirect()->to('registroConcesion');
    }

    public function insertadirectivo(Request $request)
    {
        $idDirectivo = $request->input('idDirectivo');
        $puesto = $request->input('puesto');
        $usuario = $request->input('usuario');
        $contraseña = $request->input('contraseña');

        mdirectivo::create(['idDirectivo'=>$idDirectivo,'puesto'=>$puesto,'usuario'=>$usuario,'contraseña'=>$contraseña]);
        return redirect()->to('registroDirectivo');
    }

    public function insertaseguro(Request $request)
    {
        $num_poliza = $request->input('num_poliza');
        $beneficiario = $request->input('beneficiario');
        $fecha_c = $request->input('fecha_c');
        $fecha_v = $request->input('fecha_v');
        $id_vehiculo = $request->input('id_vehiculo');

        mseguro::create(['num_poliza'=>$num_poliza,'beneficiario'=>$beneficiario,
        'fecha_c'=>$fecha_c,'fecha_v'=>$fecha_v,'id_vehiculo'=>$id_vehiculo]);
        return redirect()->to('registroSeguro');
    }

    public function insertaentrada(Request $request)
    {
        $num_placas = $request->input('num_placas');
        $fecha = $request->input('fecha');
        $hora = $request->input('hora');
        $tipo = $request->input('tipo');
        $id_checador = $request->input('id_checador');
        $id_vehiculo = $request->input('id_vehiculo');

        mregistro::create(['num_placas'=>$num_placas,'fecha'=>$fecha,'hora'=>$hora,
        'tipo'=>$tipo,'id_checador'=>$id_checador,'id_vehiculo'=>$id_vehiculo]);
        return redirect()->to('registroEntradas');
    }

    public function insertarevision(Request $request)
    {
        $estado_fisico = $request->input('estado_fisico');
        $cromatica = $request->input('cromatica');
        $estado_mec = $request->input('hora');
        $limpieza = $request->input('tipo');
        $estado_a = $request->input('id_checador');
        $fundas_a = $request->input('id_vehiculo');
        $refaccion = $request->input('id_vehiculo');
        $observaciones = $request->input('id_vehiculo');
        $id_directivo = $request->input('id_vehiculo');
        $id_vehiculo = $request->input('id_vehiculo');

        mregistro::create(['estado_fisico'=>$estado_fisico,'cromatica'=>$cromatica,'estado_mec'=>$estado_mec,
        'limpieza'=>$limpieza,'estado_a'=>$estado_a,'fundas_a'=>$fundas_a,'refaccion'=>$refaccion,
        'observaciones'=>$observaciones,'id_directivo'=>$id_directivo,'id_vehiculo'=>$id_vehiculo]);
        return redirect()->to('registroMecanica');
    }

    public function insertachofer(Request $request)
    {
        $nombre = $request->input('nombre');
        $ap_p = $request->input('ap_p');
        $ap_m = $request->input('ap_m');
        $fecha_n = $request->input('fecha_n');
        $sexo = $request->input('sexo');
        $curp = $request->input('curp');
        $domicilio = $request->input('domicilio');
        $telefono_ch = $request->input('telefono_ch');
        $telefono_e = $request->input('telefono_e');
        $escolaridad = $request->input('escolaridad');
        $foto_ch = $request->input('foto_ch');
        $tipo_licencia = $request->input('tipo_licencia');
        $num_licencia = $request->input('num_licencia');
        $antiguedad = $request->input('antiguedad');
        $status = $request->input('status');
        $id_vehiculo = $request->input('id_vehiculo');

        mchofer::create(['nombre'=>$nombre,'ap_p'=>$ap_p,'ap_m'=>$ap_m,'fecha_n'=>$fecha_n,'sexo'=>$sexo,'curp'=>$curp,
        'domicilio'=>$domicilio,'telefono_ch'=>$telefono_ch,'telefono_e'=>$telefono_e,'escolaridad'=>$escolaridad,
        'foto_ch'=>$foto_ch,'tipo_licencia'=>$tipo_licencia,'num_licencia'=>$num_licencia,'antiguedad'=>$antiguedad,
        'status'=>$status,'id_vehiculo'=>$id_vehiculo]);
        return redirect()->to('registroChofer');
    }

    public function insertaexamen(Request $request)
    {
        $producto = $request->input('producto');
        $precio = $request->input('precio');
        $cantidad = $request->input('cantidad');
        $descuento = $request->input('descuento');
        $preciofinal = $request->input('preciofinal');

        mexamen::create(['producto'=>$producto,'precio'=>$precio,'cantidad'=>$cantidad,
                        'descuento'=>$descuento,'preciofinal'=>$preciofinal]);

        return redirect()->to('examen');
    }

    public function eliminaexamen($id)
        {
            $uno = mexamen::where('id',$id)->take(1)->first();//primera opcion para editar un campo

            //$dos = modelop3::find($id);//segunda opcion para modificar un dato

            return view('vistas\exaelimina')//modificarp3 es la vista para ver los datos solicitados
                ->with('uno', $uno);
        }

        public function actualizar($id)
        {
            $editar = mexamen::find($id);

            $editar->delete();//delete los elima permanentemente

            return redirect()->to('examen');//(verdatosp4 es una ruta para mostrar los datos en general de la tabla)
        }

}

?>
