<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAX\Sexo;
use App\Models\AJAX\Alumnos;
use App\Models\AJAX\Materias;
use App\Models\AJAX\Semestre;
use App\Models\AJAX\alumnop2;


class AjaxController extends Controller
{
    /////////////////////-EJEMPLO1-//////////////////////
    public function formu()
    {
        $enviar = Sexo::pluck('sexo','id');
        return view('AJAX/ejemplo1')->with('sex',$enviar);
    }

    public function listado_alumnos($genero)
    {
        $al = Alumnos::select('id','nombre_completo','sexo')
        ->where('sexo',$genero)
        ->get();
          return $al;
    }
    //////////////////////-EJEMPLO2-/////////////////////
    public function formuSemestre()
    {
        $enviar = Semestre::pluck('semestre','id');
        return view('AJAX/ejemplo2')->with('sem',$enviar);
    }

    public function listado_semestre($semestre)
    {
        $al = Materias::select('id','materia','semestre')
        ->where('semestre',$semestre)
        ->get();
          return $al;
    }

    public function insertar(Request $request)
    {
        $id = 0;
        $nombre = $request->input('nombre');
        $n_control = $request->input('n_control');
        $semestre = $request->input('idsemestre');
        $materia = $request->input('idmateria'); 

       // $todos = $request->all();
        alumnop2::create(['id' => 0,'nombre' => $nombre,'n_control' => $n_control,
      'semestre' => $semestre,'materia' => $materia]); 
      //alumno::create($request->all());
      return redirect()->to('ajaxSemestre');
    }
    ////////////////////////////////////////////
}
?>