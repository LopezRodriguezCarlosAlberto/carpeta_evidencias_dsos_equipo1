<?php

Route::get('/', function () {
    return view('welcome');
});


//AJAX

Route::get('lista_materias/{materias}','AJAx\AjaxController@listado_materias');
Route::get('ajax','AJAx\AjaxController@formu');
Route::get('lista_materias/{materias}','AJAx\AjaxController@listado_materias');
Route::get('insertar','AJAx\AjaxController@formu');
Route::post('insertar','AJAx\AjaxController@insertar');
