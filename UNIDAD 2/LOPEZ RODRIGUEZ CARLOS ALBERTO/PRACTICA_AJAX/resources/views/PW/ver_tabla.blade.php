<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table border=1>
         <tr>
               <td>ID</td>
               <td>Nombre</td>
               <td colspan="3" align="center">Accion</td>
         </tr>
         
   @foreach($usuario as $c)
         <tr>
               <td>{{ $c->id }}</td>
               <td>{{ $c->nombre }}</td>
               <td><a href="actualizar/{{ $c->id }}">[Editar]</a></td>
               <td><a href="eliminar/{{ $c->id }}">[Eliminar total]</a></td>
               <td><a href="eliminar_bandera/{{ $c->id }}">[Eliminar bandera]</a></td>
         </tr>
   @endforeach
</table>
    
</body>
</html>