<?php

namespace App\Models\PW;

use Illuminate\Database\Eloquent\Model;

class Practica_Modelo extends Model
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected  $table = 'practicas';
    // aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    // aqui los elementos a mostrarse de la tabla en cuestion
    protected $fillable = [
      'id','nombre','bandera'
    ];
}
