<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

//use App\Models\AJAX\Sexo;
//use App\Models\AJAX\Alumnos;
//use App\Models\AJAX\materia;
use App\Models\AJAX\empresa;
use App\Models\AJAX\producto;


class empresaController extends Controller
{
    public function listado_materias($materias)
    {
        $al = materia::select('id','nombre','semestre')
        ->where('semestre',$materias)
        ->get();
          return $al;
    }

    public function formu()
    {
        $enviar = semestre::pluck('semestre','id');
        return view('AJAX/materia')->with('semestre',$enviar);
    }

    public function insertar(Request $request)
    {
        $id = 0;
        $rfc = $request->input('rfc');
        $razonsocial = $request->input('razonsocial');
        $direccionfiscal = $request->input('direccionfiscal');
        $apoderado = $request->input('apoderado'); 
        $telefono = $request->input('telefono');
       
        empresa::create(['id' => 0,'rfc' => $rfc,'razonsocial' => $razonsocial,
      'direccionfiscal' => $direccionfiscal,'apoderado' => $apoderado,'telefono' => $telefono]); 
     
      return redirect()->to('insertarempresa');
    }
}
?>