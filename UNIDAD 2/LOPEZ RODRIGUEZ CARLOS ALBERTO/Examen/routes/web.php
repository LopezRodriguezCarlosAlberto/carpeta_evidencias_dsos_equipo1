<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('Hola', 'PW_Controller\micontrolador@index');

Route::get('/', function () {
    return view('welcome');
});

Route::get('insertarProducto', 'AJAx\AbarrotesC@openInsertFormProducto');
Route::get('insertarEmpresa', 'AJAx\AbarrotesC@openInsertFormEmpresa');


Route::get('verProductos', 'AJAx\AbarrotesC@openViewListaProductos');

Route::post('postNewEmpresa', 'AJAx\AbarrotesC@doPostNewEmpresa');
Route::post('postNewProducto', 'AJAx\AbarrotesC@doPostNewProducto');

Route::get('editarEmpresa/{id}', 'AJAx\AbarrotesC@openEditFormEmpresa');
Route::put('putUpdateEmpresa/{id}', 'AJAx\AbarrotesC@doUpdateEmpresa');

Route::get('borrarEmpresa/{id}', 'AJAx\AbarrotesC@openDeleteFormEmpresa');
Route::put('putDeleteEmpresa/{id}', 'AJAx\AbarrotesC@doDeleteEmpresa');

Route::get('listaEmpresa', 'AJAx\AbarrotesC@openViewListaEmpresa');

Route::get('venta', 'AJAx\AbarrotesC@openViewVenta');

Route::get('JSON/getPrecios/{id}', 'AJAx\AbarrotesC@getPreciosProducto');