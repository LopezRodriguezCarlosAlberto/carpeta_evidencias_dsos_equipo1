<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Ferreteria




Route::get('formulario','PW_controller\Controlador_ferreteria@ver_formulario');

Route::post('insertar3','PW_controller\Controlador_ferreteria@insertar3');

Route::get('visualizar','PW_controller\Controlador_ferreteria@ver_datos');

Route::get('actualizar/{id}','PW_controller\Controlador_ferreteria@edit_datos');

Route::put('actualizar_datos/{id}','PW_Controller\Controlador_ferreteria@actualizar_datos');

Route::get('verdatos','PW_controller\Controlador_ferreteria@ver_datos');

Route::post('insertar','PW_controller\Controlador_ferreteria@insertar');

Route::get('formulario','PW_controller\Controlador_ferreteria@ver_formulario');

Route::get('actualizar/{id}','PW_controller\Controlador_ferreteria@edit_datos');

Route::put('actualizar_datos/{id}','PW_Controller\Controlador_ferreteria@actualizar_datos');

Route::get('verdatos','PW_controller\Controlador_ferreteria@ver_datos');

Route::get('eliminar/{id}','PW_controller\Controlador_ferreteria@edit_datos_eliminar');

Route::put('eliminar_datos/{id}','PW_Controller\Controlador_ferreteria@eliminar_datos');

Route::get('eliminar_bandera/{id}','PW_controller\Controlador_ferreteria@editar_bandera');

Route::put('bamdera_eliminar/{id}','PW_Controller\Controlador_ferreteria@bandera_eliminado');

Route::get('tabla','PW_controller\Controlador_ferreteria@ver_tabla');









Route::get('/', function () {
    return view('welcome');
});
