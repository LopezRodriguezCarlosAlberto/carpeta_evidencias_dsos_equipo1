<?php
    namespace app\Http\Controllers\PW_controller;
    
    use Illuminate\Http\Request;


    use App\Http\Controllers\Controller;

    use App\Models\PW\ferreteria_modelo; //Direccion de la clase (modelo)

    class Controlador_ferreteria extends Controller
    {
        public function ver_datos(){

            $rama = ferreteria_modelo::
            select('id','razonsocial','giro','domiciliofiscal','rfc','estado','fechaingreso',
            'bandera')
            ->take(1)->first();

            return view('PW/ver_ferreteria')->with('variable',$rama); //cambiar ruta
        }

        public function edit_datos($id){
            $uno = ferreteria_modelo::
            where('id',$id)->take(1)->first();
            

            return view('PW/actualizar_datos_ferreteria') //cambiar ruta
                    ->with('uno',$uno);
        }

        public function actualizar_datos(Request $data,$id)
        {
          $editar = ferreteria_modelo::find($id);
    
          //$editar->id = $data->id;
          $editar->razonsocial = $data->razonsocial;
          $editar->giro = $data->giro;
          $editar->domiciliofiscal = $data->domiciliofiscal;
          $editar->rfc= $data->rfc;
          $editar->estado = $data->estado;
          $editar->fechaingreso = $data->fechaingreso;
          $editar->bandera = "1";
          
    
          $editar->save();
    
          return redirect()->to('verdatos');
    
        }

      

        public function insertar(request $request){
            //$id = $request->input('id');
            $razonsocial  = $request->input('razonsocial');
            $giro = $request->input('giro');
            $domiciliofiscal = $request->input('domiciliofiscal');
            $rfc = $request->input('rfc');
            $estado = $request->input('estado');
            $fechaingreso = $request->input('fechaingreso');
            $bandera = '1';
            
        

            ferreteria_modelo::create(['razonsocial'=> $razonsocial,
            'giro'=> $giro,
            'domiciliofiscal'=> $domiciliofiscal, 'rfc'=> $rfc,
            'estado'=> $estado,'fechaingreso'=> $fechaingreso,
            'bandera'=> $bandera]);

            return redirect()->to('verdatos'); //cambiar ruta

        }

        public function ver_formulario(){
        return  view('PW/ingresar_ferreteria'); //cambiar ruta
        } 


        public function edit_datos_eliminar($id){
            $uno = ferreteria_modelo::
            where('id',$id)->take(1)->first();
            

            return view('PW/eliminar_datos_ferreteria') //cambiar ruta
                    ->with('uno',$uno);
        }

        public function eliminar_datos(Request $data,$id)
        {
          $editar = ferreteria_modelo::find($id);
    
          $editar->delete();
    
          return redirect()->to('verdatos');
    
        }


        public function editar_bandera($id){
            $uno = ferreteria_modelo::
            where('id',$id)->take(1)->first();
            

            return view('PW/ver_datos_bandera') //cambiar ruta
                    ->with('uno',$uno);
        }

        public function bandera_eliminado($id)
        {
          $editar = ferreteria_modelo::find($id);
    
          $editar->bandera = '0';
         
          $editar->save();
    
          return redirect()->to('verdatos');
        }
 // -------------------------------------------------------------------
        public function ver_tabla(){

            $uno = ferreteria_modelo::
            where('bandera','1')->get();

            return view('PW/ver_tabla_ferreteria')
            ->with('usuario',$uno);
        }
// -------------------------------------------------------------------
    }
    ?>