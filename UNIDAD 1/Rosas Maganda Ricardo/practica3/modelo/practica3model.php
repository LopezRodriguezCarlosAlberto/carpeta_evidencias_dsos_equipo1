<?php
//PARA LA BASE DE DATOS
namespace App\Models\PW;

use Illuminate\Database\Eloquent\Model;

class practica3model extends Model
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'tienda';
    //aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    //aqui van los elementos a mostrarse en cuestion
    protected $fillable = [
        'id','razon_social','rfc','nombre_duenio','direc_duenio','tipo','telefono','fecha_ingreso'
    ];
}
?>