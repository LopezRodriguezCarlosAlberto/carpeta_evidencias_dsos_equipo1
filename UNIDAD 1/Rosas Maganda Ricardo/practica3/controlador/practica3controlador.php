<?php

namespace app\Http\Controllers\Dsos_controller;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\PW\practica3model; //PARA LA BASE DE DATOS

class practica3controlador extends Controller
	{
		public function ver_nuevosdatos()
		{
			$practica2 = practica3model:: //Ejemplo_modelo
				select('id','razon_social','rfc','nombre_duenio','direc_duenio','tipo','telefono','fecha_ingreso')->take(1)->first();

				return view('DSOS/practica3')->with('variable',$practica2);
		}

		public function ver_formulario()
		{
			return view("DSOS\insertar_datos3");
		}

		public function insertar(request $request)
		{
            $razon_s = $request->input('razon_social');
            $rfc = $request->input('rfc');
            $nombre_d = $request->input('nombre_duenio');
            $direc = $request->input('direc_duenio');
            $tipo = $request->input('tipo');
            $tel = $request->input('telefono');
			$fecha = $request->input('fecha_ingreso');
			
            practica3model::create(['razon_social'=>$razon_s,'rfc'=>$rfc,'nombre_duenio'=>$nombre_d,
			'direc_duenio'=>$direc,'tipo'=>$tipo,'telefono'=>$tel,'fecha_ingreso'=>$fecha]);
			
			return redirect()->to('formulariolleno2');
		}
	}
?>