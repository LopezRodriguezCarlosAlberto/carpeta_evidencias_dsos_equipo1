using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Dsos
{
    #region Practica
    public class Practica
    {
        #region Member Variables
        protected int _id;
        protected string _nombre;
        protected string _aPaterno;
        protected string _aMaterno;
        protected int _edad;
        protected string _direc;
        protected int _tel;
        #endregion
        #region Constructors
        public Practica() { }
        public Practica(string nombre, string aPaterno, string aMaterno, int edad, string direc, int tel)
        {
            this._nombre=nombre;
            this._aPaterno=aPaterno;
            this._aMaterno=aMaterno;
            this._edad=edad;
            this._direc=direc;
            this._tel=tel;
        }
        #endregion
        #region Public Properties
        public virtual int Id
        {
            get {return _id;}
            set {_id=value;}
        }
        public virtual string Nombre
        {
            get {return _nombre;}
            set {_nombre=value;}
        }
        public virtual string APaterno
        {
            get {return _aPaterno;}
            set {_aPaterno=value;}
        }
        public virtual string AMaterno
        {
            get {return _aMaterno;}
            set {_aMaterno=value;}
        }
        public virtual int Edad
        {
            get {return _edad;}
            set {_edad=value;}
        }
        public virtual string Direc
        {
            get {return _direc;}
            set {_direc=value;}
        }
        public virtual int Tel
        {
            get {return _tel;}
            set {_tel=value;}
        }
        #endregion
    }
    #endregion
}