-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-02-2019 a las 23:23:58
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dsos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `practica2`
--

CREATE TABLE `practica2` (
  `id` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `aPaterno` text NOT NULL,
  `aMaterno` text NOT NULL,
  `edad` int(11) NOT NULL,
  `direc` text NOT NULL,
  `tel` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `practica2`
--

INSERT INTO `practica2` (`id`, `nombre`, `aPaterno`, `aMaterno`, `edad`, `direc`, `tel`) VALUES
(1, 'jose', 'jose', 'rios', 21, 'atzompa', 95119508),
(2, 'erick', 'andrade', 'revilla', 22, 'la cascada', 95149558),
(3, 'ricardo', 'rosas', 'maganda', 21, 'fovissste', 951532353),
(4, 'juan', 'Hernandez', 'Perez', 18, 'Alamedas 32', 951421263),
(5, 'jose', 'perez', 'lopez', 23, 'oaxaca', 9123123),
(6, 'juan', 'rodriguez', 'martinez', 34, 'puebla', 821312124),
(7, 'armando', 'juan', 'lope', 67, 'mexico', 98123123),
(8, 'alma', 'lopez', 'lopez', 12, 'usa', 95123123),
(9, 'jose', 'rios ', 'lopez', 21, 'privada del futuro', 95123123),
(10, 'lucas', 'lopez', 'rojas', 92, 'calle nueva', 91231231);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `practica2`
--
ALTER TABLE `practica2`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
