<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/hola','Dsos_controller\micontrolador@index');

Route::get('/inicio','Dsos_controller\micontrolador@indexinicio');

//PARA LA BASE DE DATOS
Route::get('ver_datos','Dsos_controller\micontrolador@ver_datos');

//PARA LA BASE DE DATOS CON BLADE
Route::get('visualizar','Dsos_controller\micontrolador@ver_nuevosdatos');

//PARA LA BASE DE DATOS CON BLADE practica 2
Route::get('practica2','Dsos_controller\practica2controlador@ver_nuevosdatos');

Route::get('/', function () {
    return view('welcome');
});
