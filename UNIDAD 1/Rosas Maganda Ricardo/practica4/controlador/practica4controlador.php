<?php

namespace app\Http\Controllers\Dsos_controller;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\PW\practica4model; //PARA LA BASE DE DATOS

class practica4controlador extends Controller
	{
        //PARA LA BASE DE DATOS CON BLADE
        public function ver_datos()
        {
            $uno = practica4model::
            select('id','nombre','aPaterno','aMaterno','edad','direc','tel')->take(1)->first();

            return view('DSOS/practica4')->with('uno',$uno);
        }

		public function ver_formulario()
		{
			return view('DSOS/insertar_datos4');
        }
        
        public function insertar(request $request)
		{
			$id = $request->input('id');
			$nombre = $request->input('nombre');
			$aPaterno = $request->input('aPaterno');
			$aMaterno = $request->input('aMaterno');
			$edad = $request->input('edad');
			$direc = $request->input('direc');
			$tel = $request->input('tel');

			practica4model::create(['id'=>$id,'nombre'=>$nombre,'aPaterno'=>$aPaterno,'aMaterno'=>$aMaterno,'edad'=>$edad,'direc'=>$direc,'tel'=>$tel]);
			return redirect()->to('visualizar4'); //insertar
		}

        public function edit_datos($id)
        {
            $uno = practica4model::
            where('id',$id)->take(1)->first(); //primera manera
            //$dos = practica4model::find($id); //segunda manera

            return view ('DSOS/actualizar_datos4')->with ('uno', $uno);
        }

        public function actualizar_datos(Request $data,$id)
        {
            $editar = practica4model::find($id);

            $editar->id = $data->id;
            $editar->nombre = $data->nombre;
            $editar->aPaterno = $data->aPaterno;
            $editar->aMaterno = $data->aMaterno;
            $editar->edad = $data->edad;
            $editar->direc = $data->direc;
            $editar->tel = $data->tel;

            $editar->save();

            return redirect()->to('visualizar4');
    }

	}
?>