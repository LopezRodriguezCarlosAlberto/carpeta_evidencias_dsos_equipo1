<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/hola','Dsos_controller\micontrolador@index');

Route::get('/inicio','Dsos_controller\micontrolador@indexinicio');

Route::get('/', function () {
    return view('welcome');
});
