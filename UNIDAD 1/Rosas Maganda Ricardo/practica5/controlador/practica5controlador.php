<?php

namespace app\Http\Controllers\Dsos_controller;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\PW\practica5model; //PARA LA BASE DE DATOS

class practica5controlador extends Controller
	{
        public function ver_formulario()
		{
			return view('DSOS/insertar_datos5');
        }

        public function insertar(request $request)
		{
			$rfc = $request->input('rfc');
			$curp = $request->input('curp');
			$ncontrol = $request->input('ncontrol');
			$materia1 = $request->input('materia1');
			$materia2 = $request->input('materia2');
            $materia3 = $request->input('materia3');
            $calif1 = $request->input('calif1');
            $calif2 = $request->input('calif2');
            $calif3 = $request->input('calif3');
            $promedio = $request->input('promedio');
            $fecha = $request->input('fecha');

            practica5model::create(['rfc'=>$rfc,'curp'=>$curp,'ncontrol'=>$ncontrol,
            'materia1'=>$materia1,'materia2'=>$materia2,'materia3'=>$materia3,'calif1'=>$calif1,
            'calif2'=>$calif2,'calif3'=>$calif3,'promedio'=>$promedio,'fecha'=>$fecha]);

			return redirect()->to('insertar_practica5'); //insertar
        }
        
        public function edit_datos($id)
        {
            $uno = practica5model::
            where('id',$id)->take(1)->first();

            return view ('DSOS/actualizar_datos5')->with ('uno', $uno);
        }

        public function actualizar_datos(Request $data,$id)
        {
            $editar = practica5model::find($id);

            $editar->id = $data->id;
            $editar->rfc = $data->rfc;
            $editar->curp = $data->curp;
            $editar->ncontrol = $data->ncontrol;
            $editar->materia1 = $data->materia1;
            $editar->materia2 = $data->materia2;
            $editar->materia3 = $data->materia3;
            $editar->calif1 = $data->calif1;
            $editar->calif2 = $data->calif2;
            $editar->calif3 = $data->calif3;
            $editar->promedio = $data->promedio;
            $editar->fecha = $data->fecha;

            $editar->save();

            return redirect()->to('insertar_practica5');
    }

    }