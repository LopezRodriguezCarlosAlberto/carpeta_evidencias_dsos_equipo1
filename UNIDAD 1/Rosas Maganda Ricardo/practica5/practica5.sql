-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-02-2019 a las 19:27:55
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dsos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `practica5`
--

CREATE TABLE `practica5` (
  `id` int(11) NOT NULL,
  `rfc` text NOT NULL,
  `curp` text NOT NULL,
  `ncontrol` int(11) NOT NULL,
  `materia1` text NOT NULL,
  `materia2` text NOT NULL,
  `materia3` text NOT NULL,
  `calif1` int(11) NOT NULL,
  `calif2` int(11) NOT NULL,
  `calif3` int(11) NOT NULL,
  `promedio` double NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `practica5`
--

INSERT INTO `practica5` (`id`, `rfc`, `curp`, `ncontrol`, `materia1`, `materia2`, `materia3`, `calif1`, `calif2`, `calif3`, `promedio`, `fecha`) VALUES
(2, 'HEGE990414KM5', 'HEGE990414HMCRRS07', 14162439, 'Español', 'Inglés', 'Matematicas', 9, 9, 9, 9, '2019-02-26'),
(3, 'HEHE990414KM5', 'HEHE990414HMCRRS07', 14162839, 'Matematicas', 'Español', 'Inglés', 9, 9, 9, 9, '2019-02-26'),
(4, 'ROMM950125EQ7', 'ROMM950125MMCSGR02', 16173563, 'Español', 'Inglés', 'Matematicas', 8, 9, 10, 9, '2019-02-22'),
(5, 'ROMR970415BL5', 'ROMR970415HYNSGL01', 15161718, 'Inglés', 'Matematicas', 'Español', 10, 10, 10, 10, '2019-02-20'),
(6, 'JFFOR40', 'MFDL42', 15163842, 'Español', 'Matematicas', 'Ingles', 10, 10, 10, 10, '2019-02-21');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `practica5`
--
ALTER TABLE `practica5`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `practica5`
--
ALTER TABLE `practica5`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
