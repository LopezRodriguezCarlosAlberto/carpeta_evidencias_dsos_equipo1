<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/hola','Dsos_controller\micontrolador@index');

Route::get('/inicio','Dsos_controller\micontrolador@indexinicio');

//PARA LA BASE DE DATOS
Route::get('ver_datos','Dsos_controller\micontrolador@ver_datos');

//PARA LA BASE DE DATOS CON BLADE
Route::get('visualizar','Dsos_controller\micontrolador@ver_nuevosdatos');

//PARA LA BASE DE DATOS CON BLADE practica 2
Route::get('practica2','Dsos_controller\practica2controlador@ver_nuevosdatos2');

//PARA REGISTRAR EN UN FORMUALARIO PRACTICA 2
Route::get('formulario','Dsos_controller\practica2controlador@ver_formulario');
Route::post('insertar','Dsos_controller\practica2controlador@insertar');
Route::get('formulariolleno','Dsos_controller\practica2controlador@ver_nuevosdatos');

//PARA REGISTRAR EN UN FORMUALARIO PRACTICA 3
Route::get('formulario2','Dsos_controller\practica3controlador@ver_formulario');
Route::post('insertar2','Dsos_controller\practica3controlador@insertar');
Route::get('formulariolleno2','Dsos_controller\practica3controlador@ver_nuevosdatos');

//PARA ACTUALIZAR DATOS PRACTICA 4
Route::get('visualizar4','Dsos_controller\practica4controlador@ver_datos'); //FORMULARIO CON EL PRIMER REGISTRO
Route::get('formulario','Dsos_Controller\practica4controlador@ver_formulario'); //FORMULARIO PARA AGREGAR UN NUEVO REGISTRO
Route::post('insertar4','Dsos_Controller\practica4controlador@insertar'); //BOTON DE INSERTAR
Route::get('actualizar/{id}','Dsos_controller\practica4controlador@edit_datos'); //CONSULTAR DEACUERDO AL ID
Route::put('actualizar_datos/{id}','Dsos_controller\practica4controlador@actualizar_datos'); //BOTON DE ACTUALIZAR CAMBIANDO DATOS

//PRACTICA 4 ENVIO MATERIAS-ALUMNOS
Route::get('insertar_practica5','Dsos_controller\practica5controlador@ver_formulario');//FORMULARIO PARA AGREGAR UN NUEVO REGISTRO
Route::post('insertar5','Dsos_Controller\practica5controlador@insertar'); //BOTON DE INSERTAR
Route::get('editar_practica5/{id}','Dsos_controller\practica5controlador@edit_datos'); //CONSULTAR DEACUERDO AL ID
Route::put('actualizar_datos/{id}','Dsos_controller\practica5controlador@actualizar_datos'); //BOTON DE ACTUALIZAR CAMBIANDO DATOS

Route::get('/', function () {
    return view('welcome');
}); 