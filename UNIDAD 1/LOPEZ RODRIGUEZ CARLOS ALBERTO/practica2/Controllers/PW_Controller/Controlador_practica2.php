<?php
    namespace app\Http\Controllers\PW_controller;
    
    use Illuminate\Http\Request;

    use App\Http\Controllers\Controller;

    use App\Models\PW\practica2_modelo; //Direccion de la clase (modelo)

    class Controlador_practica2 extends Controller
    {
        


        public function ver_datos(){

            

            $rama = practica2_modelo::
            select('id','nombrealumno','apellidopaterno','apellidomaterno','edad','direccion','telefono')->take(1)->first();

            return view('PW/ver_practica2')->with('variable',$rama);
        }

        
    }
    ?>