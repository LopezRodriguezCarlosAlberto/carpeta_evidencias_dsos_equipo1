<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('ver_practica2','PW_controller\Controlador_practica2@ver_datos');


Route::get('/', function () {
    return view('welcome');
});
