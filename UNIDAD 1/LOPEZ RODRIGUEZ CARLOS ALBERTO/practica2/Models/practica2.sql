-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-02-2019 a las 23:23:58
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dsos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `practica2`
--

CREATE TABLE `escuela` (
  `id` int(11) NOT NULL,
  `nombrealumno` text NOT NULL,
  `apelldiopaterno` text NOT NULL,
  `apelldiomaterno` text NOT NULL,
  `edad` int(11) NOT NULL,
  `direccion` text NOT NULL,
  `telefono` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `practica2`
--

INSERT INTO `practica2` (`id`, `nombrealumno`, `apelldiopaterno`, `apelldiomaterno`, `edad`, `direccion`, `telefono`) VALUES
(1, 'angela', 'perez', 'santiago', 33, 'lomas', 95119508),
(2, 'cinti', 'lopez', 'santiago', 24, 'esmeralda', 95149558),
(3, 'mario', 'martinez', 'pedro', 19, 'rosario', 951532353),
(4, 'andrea', 'Hernandez', 'Perez', 45, 'xoxo', 951421263),
(5, 'vanesa', 'perez', 'robledo', 23, '5 etapa', 9123123),
(6, 'esmeralda', 'rodriguez', 'nieto', 44, 'puebla', 821312124),
(7, 'azucena', 'perez', 'lopez', 35, 'distrito federal', 98123123),
(8, 'cristal', 'lopez', 'rodriguez', 36, 'queretaro', 95123123),
(9, 'pedro', 'revilla ', 'lopez', 37, 'privada del futuro', 95123123),
(10, 'ivan', 'lopez', 'sanchez', 39, 'calle atzompa', 91231231);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `practica2`
--
ALTER TABLE `escuela`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
