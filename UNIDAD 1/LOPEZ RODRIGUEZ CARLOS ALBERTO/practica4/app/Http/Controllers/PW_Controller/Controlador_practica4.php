<?php
    namespace app\Http\Controllers\PW_controller;
    
    use Illuminate\Http\Request;


    use App\Http\Controllers\Controller;

    use App\Models\PW\practica3_modelo; //Direccion de la clase (modelo)

    class Controlador_practica4 extends Controller
    {
        public function ver_datos(){

            $rama = practica3_modelo::
            select('id','razonsocial','rfc','nombredueño','direccion','tipodeempresa','telefono','fechadeingreso')
            ->take(1)->first();

            return view('PW/ver_practica4')->with('variable',$rama);
        }
        public function edit_datos($id){
            $uno = practica3_modelo::
            where('id',$id)->take(1)->first();
            $dos = practica3_modelo::find($id);

            return view('PW/actualizar_datos')
                    ->with('uno',$uno);
        }

        public function actualizar_datos(Request $data,$id)
        {
          $editar = practica3_modelo::find($id);
    
          $editar->id = $data->id;
          $editar->razonsocial = $data->razonsocial;
          $editar->rfc = $data->rfc;
          $editar->nombredueño = $data->nombredueño;
          $editar->direccion = $data->direccion;
          $editar->tipodeempresa = $data->tipodeempresa;
          $editar->telefono = $data->telefono;
          $editar->fechadeingreso = $data->fechadeingreso;
    
          $editar->save();
    
          return redirect()->to('visualizar');
    
        }

       

        public function insertar3(request $request){
            $id = $request->input('id');
            $razonsocial = $request->input('razonsocial');
            $rfc = $request->input('rfc');
            $nombredueño = $request->input('nombredueño');
            $direccion = $request->input('direccion');
            $tipodeempresa = $request->input('tipodeempresa');
            $telefono = $request->input('telefono');
            $fechadeingreso = $request->input('fechadeingreso');

            practica3_modelo::create(['id'=>$id,'razonsocial'=> $razonsocial,
            'rfc'=> $rfc, 'nombredueño'=> $nombredueño,
            'direccion'=> $direccion,'tipodeempresa'=> $tipodeempresa,
            'telefono'=> $telefono,'fechadeingreso'=> $fechadeingreso]);

            return redirect()->to('formulario3');

        }

        public function ver_formulario3(){
        return  view('PW/ver_practica3');
        } 
    }
    ?>