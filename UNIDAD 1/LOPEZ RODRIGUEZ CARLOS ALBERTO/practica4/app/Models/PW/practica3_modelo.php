<?php

namespace App\Models\PW;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model; //Variable (Model) de la libreria a invocar

class practica3_modelo extends Model //siempre la clase se tiene que llamar como el archivo
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'tienda';

    //aqui la llave primaria de la tabla
    protected $primarykey = 'id';

    //Desactivar la fecha cuando no tengas dichos campos
    public $timestamps = false;

    //aqui los elementos a mostrarse de la tabla en cuestión
    protected $fillable = [
        'id','razonsocial','rfc','nombredueño','direccion','tipodeempresa','telefono','fechadeingreso'
    ];
}


?>