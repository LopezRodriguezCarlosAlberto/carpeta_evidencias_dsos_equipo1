<?php
    namespace app\Http\Controllers\PW_controller;
    
    use Illuminate\Http\Request;


    use App\Http\Controllers\Controller;

    use App\Models\PW\practica5_modelo; //Direccion de la clase (modelo)

    class Controlador_practica5 extends Controller
    {
        public function ver_datos(){

            $rama = practica5_modelo::
            select('id','rfc','curp','numctrl','materia1','materia2','materia3',
            'calif1','calif2','calif3','promedio','fecha')
            ->take(1)->first();

            return view('PW/ver_practica5')->with('variable',$rama); //cambiar ruta
        }

        public function edit_datos($id){
            $uno = practica5_modelo::
            where('id',$id)->take(1)->first();
            

            return view('PW/actualizar_datos_practica5') //cambiar ruta
                    ->with('uno',$uno);
        }

        public function actualizar_datos(Request $data,$id)
        {
          $editar = practica5_modelo::find($id);
    
          $editar->id = $data->id;
          $editar->rfc = $data->rfc;
          $editar->curp = $data->curp;
          $editar->numctrl = $data->numctrl;
          $editar->materia1 = $data->materia1;
          $editar->materia2 = $data->materia2;
          $editar->materia3 = $data->materia3;
          $editar->calif1 = $data->calif1;
          $editar->calif2 = $data->calif2;
          $editar->calif3 = $data->calif3;
          $editar->promedio = $data->promedio;
          $editar->fecha = $data->fecha;
    
          $editar->save();
    
          return redirect()->to('verdatos');
    
        }

      

        public function insertar(request $request){
            $id = $request->input('id');
            $rfc = $request->input('rfc');
            $curp = $request->input('curp');
            $numctrl = $request->input('numctrl');
            $materia1 = $request->input('materia1');
            $materia2 = $request->input('materia2');
            $materia3 = $request->input('materia3');
            $calif1 = $request->input('calif1');
            $calif2 = $request->input('calif2');
            $calif3 = $request->input('calif3');
            $promedio = $request->input('promedio');
            $fecha = $request->input('fecha');
        

        practica5_modelo::create(['id'=>$id,'rfc'=> $rfc,'curp'=> $curp,
            'numctrl'=> $numctrl, 'materia1'=> $materia1,
            'materia2'=> $materia2,'materia3'=> $materia3,
            'calif1'=> $calif1,'calif2'=> $calif2,'calif3'=> $calif3,
            'promedio'=> $promedio,'fecha'=> $fecha]);

            return redirect()->to('verdatos'); //cambiar ruta

        }

        public function ver_formulario(){
        return  view('PW/ingresar_practica5'); //cambiar ruta
        } 
    }
    ?>