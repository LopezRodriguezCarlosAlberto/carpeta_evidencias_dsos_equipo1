<?php

namespace App\Models\PW;

use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model; //Variable (Model) de la libreria a invocar

class practica5_modelo extends Model //siempre la clase se tiene que llamar como el archivo
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'estudiante';

    //aqui la llave primaria de la tabla
    protected $primarykey = 'id';

    //Desactivar la fecha cuando no tengas dichos campos
    public $timestamps = false;

    //aqui los elementos a mostrarse de la tabla en cuestión
    protected $fillable = [
        'id','rfc','curp','numctrl','materia1','materia2','materia3',
        'calif1','calif2','calif3','promedio','fecha'
   
    ];
}


?>