<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('hola','PW_controller\Mi_controlador@index');

//Route::get('ver_datos','PW_controller\Mi_controlador@ver_datos');

//Route::get('visualizar','PW_controller\Mi_controlador@ver_datosbd');

//Route::get('ver_practica2','PW_controller\Controlador_practica2@ver_datos');

//practica 2

//Route::get('formulario','PW_controller\Controlador_practica2@ver_formulario');

//Route::post('insertar','PW_controller\Controlador_practica2@insertar');

//practica 3

Route::get('formulario3','PW_controller\Controlador_practica3@ver_formulario3');

Route::post('insertar3','PW_controller\Controlador_practica3@insertar3');

//practica 4

//Route::get('visualizar','PW_controller\Controlador_practica4@ver_datos');

//Route::get('actualizar/{id}','PW_controller\Controlador_practica4@edit_datos');

//Route::put('actualizar_datos/{id}','PW_Controller\Controlador_practica4@actualizar_datos');

//practica 5

//Route::get('verdatos','PW_controller\Controlador_practica5@ver_datos');

//Route::post('insertar','PW_controller\Controlador_practica5@insertar');

//Route::get('formulario','PW_controller\Controlador_practica5@ver_formulario');

//Route::get('actualizar/{id}','PW_controller\Controlador_practica5@edit_datos');

//Route::put('actualizar_datos/{id}','PW_Controller\Controlador_practica5@actualizar_datos');

// 

Route::get('/', function () {
    return view('welcome');
});
