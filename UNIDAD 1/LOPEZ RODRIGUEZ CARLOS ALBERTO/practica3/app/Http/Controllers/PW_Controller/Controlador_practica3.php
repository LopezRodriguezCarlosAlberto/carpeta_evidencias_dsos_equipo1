<?php
    namespace app\Http\Controllers\PW_controller;
    
    use Illuminate\Http\Request;


    use App\Http\Controllers\Controller;

    use App\Models\PW\practica3_modelo; //Direccion de la clase (modelo)

    class Controlador_practica3 extends Controller
    {
        

        public function insertar3(request $request){
            $id = $request->input('id');
            $razonsocial = $request->input('razonsocial');
            $rfc = $request->input('rfc');
            $nombredueño = $request->input('nombredueño');
            $direccion = $request->input('direccion');
            $tipodeempresa = $request->input('tipodeempresa');
            $telefono = $request->input('telefono');
            $fechadeingreso = $request->input('fechadeingreso');

            practica3_modelo::create(['id'=>$id,'razonsocial'=> $razonsocial,
            'rfc'=> $rfc, 'nombredueño'=> $nombredueño,
            'direccion'=> $direccion,'tipodeempresa'=> $tipodeempresa,
            'telefono'=> $telefono,'fechadeingreso'=> $fechadeingreso]);

            return redirect()->to('formulario3');

        }

        public function ver_formulario3(){
        return  view('PW/ver_practica3');
        } 
    }
    ?>