<?PHP

namespace App\Models\PW;

use Illuminate\Database\Eloquent\Model;

class modelop4 extends Model
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'practica4';
    //aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    //aqui los elementos a mostrarse en la tabla e cuestion
    protected $fillable = [
        'id','rfc','curp','num_ctrl','materia1','calif_m1','materia2','calif_m2','materia3','calif_m3','promedio','fecha'
    ];
}