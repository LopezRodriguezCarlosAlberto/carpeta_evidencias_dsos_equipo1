<?php
    namespace app\Http\Controllers\PW_controler;
    
    use Illuminate\Http\Request;

    use App\Http\Controllers\Controller;

    use App\Models\PW\modelop3;

    class controladorp3 extends Controller
    {
        public function datos()
        {
            $practica3 = modelop3::
            select('id','razonSocial','rfc','nombrec','direccion','tipoEmpresa','telefono','fechaIngreso')->take(1)->first();
            return view('PW/vistap3')->with('variable',$practica3);
        }

       public function verFormulariop3() 
       {
           return view ("PW\insertarp3");
       }

        public function insertardatosp3(Request $request)
        {
            $id = $request->input('id');
            $razonSocial = $request->input('razonSocial');
            $rfc = $request->input('rfc');
            $nombrec = $request->input('nombrec');
            $direccion = $request->input('direccion');
            $tipoEmpresa = $request->input('tipoEmpresa');
            $telefono = $request->input('telefono');
            $fechaIngreso = $request->input('fechaIngreso');

            modelop3::create(['id'=>$id,'razonSocial'=>$razonSocial,'rfc'=>$rfc,'nombrec'=>$nombrec,'direccion'=>$direccion,'tipoEmpresa'=>$tipoEmpresa,'telefono'=>$telefono,'fechaIngreso'=>$fechaIngreso]);
            return redirect()->to('verdatosp3');//a donde debe de ir despues de realizar la insercion
        }//(redirigir hacia la vista que muestra el primer registro)

        public function editDatos($id)
        {
            $uno = modelop3::where('id',$id)->take(1)->first();//primera opcion para editar un campo

            //$dos = modelop3::find($id);//segunda opcion para modificar un dato

            return view('PW\modificarp3')//modificarp3 es la vista para ver los datos solicitados
                ->with('uno', $uno);
        }

        public function actualizarDatos(Request $data, $id)
        {
            $editar = modelop3::find($id);

            $editar->id = $data->id;
            $editar->razonSocial = $data->razonSocial;
            $editar->rfc = $data->rfc;
            $editar->nombrec = $data->nombrec;
            $editar->direccion = $data->direccion;
            $editar->tipoEmpresa = $data->tipoEmpresa;
            $editar->telefono = $data->telefono;
            $editar->fechaIngreso = $data->fechaIngreso;

            $editar->save();

            return redirect()->to('verdatosp3');
        }

    }
    ?>