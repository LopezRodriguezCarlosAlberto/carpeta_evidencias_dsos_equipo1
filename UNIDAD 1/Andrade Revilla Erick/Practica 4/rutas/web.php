<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('hola','PW_controler\Mi_controlador@index');

Route::get('sesion','PW_controler\Mi_controlador@isesion');

//ejercicio mostrar datos de tabla
Route::get('datos','PW_controler\Mi_controlador@verDatos');//muestra el prmer registro

//Practica 2
Route::get('practica2','PW_controler\nuevocontrolador@datos');//muestra el primer registro de la tabla
Route::get('formulario','PW_controler\nuevocontrolador@verFormulario');//muestra el formulario para registrar
Route::post('insertar','PW_controler\nuevocontrolador@insertar');//es llamada por la vista insertarDatos(inserta en la tabla)

//PRACTICA 3 inserta y luego muestra el primer registro
Route::get('verdatosp3','PW_controler\controladorp3@datos');//para visualizar el primer registro que se insert
Route::get('formulariop3','PW_controler\controladorp3@verFormulariop3');//muestra el formulario
Route::post('insertardatosp3','PW_controler\controladorp3@insertardatosp3');//es llamada por la vista insertarp3 (inserta en la tabla)

//modificar/actualizar un registro
Route::get('actualizar/{id}','PW_controler\controladorp3@editDatos');//muestra el registro seleccionado
Route::put('actualizarDatos/{id}','PW_controler\controladorp3@actualizarDatos');//ejecuta la funcion para modificar el registro

//practica 4
Route::get('verdatosp4','PW_controler\controladorp4@verDatos');//ver datos de la tabla practica 4
//insertar
Route::get('formulariop4','PW_controler\controladorp4@verFormulariop4');//muestra el formulario
Route::post('insertardatosp4','PW_controler\controladorp4@insertardatosp4');//es llamada por la vista insertarp3 (inserta en la tabla)
//actualizar/modificar practica 4
Route::get('actualizarp4/{id}','PW_controler\controladorp4@editDatosp4');//muestra el registro seleccionado
Route::put('actualizarDatosp4/{id}','PW_controler\controladorp4@actualizarDatosp4');//ejecuta la funcion para modificar el registro

?>