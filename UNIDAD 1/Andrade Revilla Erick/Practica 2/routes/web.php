<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('hola','PW_controler\Mi_controlador@index');

Route::get('sesion','PW_controler\Mi_controlador@isesion');

Route::get('ver_datos','PW_controler\Mi_controlador@ver_datos');

Route::get('practica2','PW_controler\nuevocontrolador@datos');