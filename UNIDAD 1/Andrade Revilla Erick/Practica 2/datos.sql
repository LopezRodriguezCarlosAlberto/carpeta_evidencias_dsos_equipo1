-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-02-2019 a las 23:25:56
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practicas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos`
--

CREATE TABLE `datos` (
  `id` int(11) NOT NULL,
  `Nombre` text NOT NULL,
  `aPaterno` text NOT NULL,
  `aMaterno` text NOT NULL,
  `edad` int(11) NOT NULL,
  `direccion` text NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datos`
--

INSERT INTO `datos` (`id`, `Nombre`, `aPaterno`, `aMaterno`, `edad`, `direccion`, `telefono`) VALUES
(1, 'Erick', 'Andrade', 'Revilla', 22, 'AV. Lazaro cardenas', 1518034),
(2, 'Jose', 'Jose', 'Rios', 21, 'Atzompa', 1030289),
(3, 'Ricardo', 'Rosas', 'Maganda', 21, 'Reforma', 1234567),
(4, 'Saul', 'Aragon', 'Moreyra', 20, 'Atzompa', 987654321),
(5, 'Diego', 'Andrade', 'Revilla', 19, 'santa lucia del camino', 654789321),
(6, 'Dinorath', 'Andrade', 'Revilla', 24, 'bosque norte', 753196248),
(7, 'Luis', 'Zaragosa', 'Farrera', 21, 'Tacuballa', 468257912),
(8, 'Perla', 'Tapia ', 'Mojica', 19, 'Atzompa', 864275911),
(9, 'Israel', 'Morgan ', 'Martinez', 22, 'Santa rosa', 794613258),
(10, 'Marcos', 'Morales', 'Mendez', 21, 'Tlacochauaya', 963147852);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datos`
--
ALTER TABLE `datos`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
