<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
</head>
<body>
    {!!Form::label('Nombre: ')!!}
    <br>
    {!!form::text('nombre',$variable->nombre)!!}
    <br>
    {!!Form::label('Apellido Paterno: ')!!}
    <br>
    {!!form::text('aPaterno',$variable->aPaterno)!!}
    <br>
    {!!Form::label('Apellido Materno: ')!!}
    <br>
    {!!form::text('aMaterno',$variable->aMaterno)!!}
    <br>
    {!!Form::label('Edad: ')!!}
    <br>
    {!!form::Int('edad',$variable->edad)!!}
    <br>
    {!!Form::label('Direccion: ')!!}
    <br>
    {!!form::text('direccion',$variable->direccion)!!}
    <br>
    {!!Form::label('Telefono: ')!!}
    <br>
    {!!form::Int('telefono',$variable->telefono)!!}
    <br>
</body>
</html>