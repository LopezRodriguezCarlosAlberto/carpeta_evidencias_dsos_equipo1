<?PHP

namespace App\Models\PW;

use Illuminate\Database\Eloquent\Model;

class modelop3 extends Model
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'practica3';
    //aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    //aqui los elementos a mostrarse en la tabla e cuestion
    protected $fillable = [
        'id','razonSocial','rfc','nombrec','direccion','tipoEmpresa','telefono','fechaIngreso'
    ];
}