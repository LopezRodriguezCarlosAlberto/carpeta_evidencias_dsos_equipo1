<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>InsertaP3</title>
</head>
<body>
    
    {!!Form::open(array('url'=>'insertardatosp3','method'=>'POST','autocomplete'=>'off'))!!}

    {!!Form::label('Razon Social: ')!!}
    {!!Form::text('razonSocial',null)!!}
    <br>
    {!!Form::label('RFC: ')!!}
    {!!Form::text('rfc',null)!!}
    <br>
    {!!Form::label('- DUEÑO -')!!}
    <br>
    {!!Form::label('Nombre Completo: ')!!}
    {!!Form::text('nombrec',null)!!}
    <br>
    {!!Form::label('Direccion: ')!!}
    {!!Form::text('direccion',null)!!}
    <br>
    {!!Form::label('Tipo de empresa: ')!!}
    {!!Form::text('tipoEmpresa',null)!!}
    <br>
    {!!Form::label('Telefono: ')!!}
    {!!Form::text('telefono',null)!!}
    <br>
    {!!Form::label('Fecha de Ingreso: ')!!}
    {!!Form::text('fechaIngreso',null)!!}
    <br>

    {!!Form::submit('Registrar',['razonSocial'=>'grabar','rfc'=>'grabar','nombrec'=>'grabar','direccion'=>'grabar','tipoEmpresa'=>'grabar','telefono'=>'grabar','fechaIngreso'=>'grabar','content'=>'<span>Insertar</span>'])!!}

    {!!Form::close()!!}

</body>
</html>