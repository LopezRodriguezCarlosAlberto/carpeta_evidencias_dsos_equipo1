<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

//practica 03
Route::get('formularioTienda','PW_controller\practica3@verformulario');
Route::post('insertarTienda','PW_controller\practica3@insertardatos');
Route::get('practica3','PW_controller\practica3@ver_nuevosdatos2');

Route::get('/', function () {
    return view('welcome');
});
