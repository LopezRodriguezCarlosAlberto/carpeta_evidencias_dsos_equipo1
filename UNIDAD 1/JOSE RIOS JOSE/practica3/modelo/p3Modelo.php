<?php
     //15/02/2019 
     //practica 3  
namespace App\Models\PW;
use Illuminate\Database\Eloquent\Model;

class p3Modelo extends Model{
   //aqui se declara el nombre de la tabla que esta en mysql
    protected  $table = 'tienda';

    //aqui la llave primario de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;

    //aqui los elementos a mostarse de la tabla en cuestion 
    protected $fillable =[
        'id','Razon_Social','RFC','Nombre_Dueño','direccion','tipo_empresa','telefono','fecha_ingreso'
    ];


}
