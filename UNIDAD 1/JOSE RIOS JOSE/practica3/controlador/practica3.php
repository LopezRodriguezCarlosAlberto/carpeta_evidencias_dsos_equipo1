<?php

namespace app\Http\Controllers\PW_controller;

use Illuminate\Http\Request;
 
use App\Http\Controllers\Controller;

use App\Models\PW\p3Modelo; //PARA LA BASE DE DATOS


class practica3 extends Controller
	{
		//PARA LA BASE DE DATOS CON BLADE
		public function ver_nuevosdatos2()
		{
			$practica3 = p3Modelo:: //Ejemplo_modelo
				select( 'id','Razon_Social','RFC','Nombre_Dueño','direccion','tipo_empresa','telefono','fecha_ingreso')->orderBy('id_tienda', 'desc')->take(1)->first();

				return view('PW/practica3')->with('variable',$practica3);
		}
        //14/02/2019
		public function insertardatos(Request $request){
			$razon = $request->input('Razon_Social');
			$rfc = $request->input('RFC');
			$nombre = $request->input('Nombre_Dueño');
			$direccion= $request->input('direccion');
			$tipo = $request->input('tipo_empresa');
            $tele = $request->input('telefono');
			$fecha = $request->input('fecha_ingreso');

			p3Modelo::create(['Razon_Social'=> $razon,'RFC'=> $rfc,'Nombre_Dueño'=> $nombre,'direccion'=> $direccion,'tipo_empresa'=> $tipo,'telefono'=> $tele,'fecha_ingreso'=> $fecha]);
			return redirect()->to('practica3');
		}

		public function verformulario(){
			return view('PW/registraP3');
		}


	}
?>