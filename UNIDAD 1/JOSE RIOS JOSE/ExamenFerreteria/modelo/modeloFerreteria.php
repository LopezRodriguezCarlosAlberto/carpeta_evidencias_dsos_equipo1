<?php
//PARA LA BASE DE DATOS
namespace App\Models\PW;

use Illuminate\Database\Eloquent\Model;

class modeloFerreteria extends Model
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'ferreteria';
    //aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    //aqui van los elementos a mostrarse en cuestion
    protected $fillable = [
        'id','Razon_Social','Giro','Domicilio_Fiscal','RFC','Estado','Año_ingreso'
    ];
}
?>