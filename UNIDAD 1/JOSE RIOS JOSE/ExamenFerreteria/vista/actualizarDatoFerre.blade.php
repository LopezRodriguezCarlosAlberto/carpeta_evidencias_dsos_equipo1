<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
        input{
         display: block;
         border: 1px solid #ccc;
        box-sizing: border-box;
        }
        form{
            padding:2px 0 10px 0;
            cursor:pointer;
            display:block;
            width:220px;
            margin: 0 auto;
            text-aling:left;
        }
        </style>
  </head>
    <body>
      {!!Form::open(array('url'=>'actualizar_datosFerre/'.$uno->id,'method'=>'PUT','autocomplete'=>'off'))!!}
      
            {!!Form::label('Razon Social:') !!}
            {!!form::text('rSocial',$uno->Razon_Social) !!}
            <br>
            {!!Form::label('Giro:') !!}
            {!!form::text('giro',$uno->Giro) !!}
            <br>
            {!!Form::label('Domicilio Fiscal:') !!}
            {!!form::text('dfiscal',$uno->Domicilio_Fiscal) !!}
            <br>     
            {!!Form::label('RFC:') !!}
            {!!form::text('rfc',$uno->RFC) !!}
            <br>
            {!!Form::label('Estado:') !!}
            {!!form::text('estado',$uno->Estado) !!}
            <br>
            {!!Form::label('Año Ingreso: ') !!}
             {!!Form::date('fecha',$uno->Año_ingreso)!!}


        <br><br>

        {!!Form::submit('Actualizar',['rfc'=>'grabar','curp'=>'grabar',
            'ncontrol'=>'grabar','materia1'=>'grabar','materia2'=>'grabar','materia3'=>'grabar',
            'calif1'=>'grabar','calif2'=>'grabar','calif3'=>'grabar','promedio'=>'grabar','fecha'=>'grabar',
            'content'=>'<span>Registrar<span>'])!!}

        {!!Form::close()!!}

  </body>
</html>