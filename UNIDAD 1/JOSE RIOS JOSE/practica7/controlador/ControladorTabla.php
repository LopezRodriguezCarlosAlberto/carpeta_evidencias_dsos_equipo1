<?php
    namespace app\Http\Controllers\PW_controller;
    
    use Illuminate\Http\Request;

    use App\Http\Controllers\Controller;

    use App\Models\PW\ModeloP5; //Direccion de la clase (modelo)

    class ControladorTabla extends Controller
    {
        public function edit_datos($id){
            $uno = ModeloP5::
            where('id',$id)->take(1)->first();
            return view('PW/actualizardatotabla') //cambiar ruta
                    ->with('uno',$uno);
        }

        public function actualizar_datos(Request $data,$id)
        {
          $editar = ModeloP5::find($id);
    
          $editar->id = $data->id;
          $editar->rfc = $data->rfc;
          $editar->curp = $data->curp;
          $editar->ncontrol = $data->ncontrol;
          $editar->materia1 = $data->materia1;
          $editar->materia2 = $data->materia2;
          $editar->materia3 = $data->materia3;
          $editar->calif1 = $data->calif1;
          $editar->calif2 = $data->calif2;
          $editar->calif3 = $data->calif3;
          $editar->promedio = $data->promedio;
          $editar->fecha = $data->fecha;
    
          $editar->save();
    
          return redirect()->to('tabla');
    
        }

        public function edit_datos_eliminar($id){
            $editar = ModeloP5::find($id);
            $editar->delete();
            return redirect()->to('tabla');
        }
        
        public function editar_bandera($id){
            $uno = ModeloP5::
            where('id',$id)->take(1)->first();
            $uno->estado_Visual = 0;
            $uno->save();
            return redirect()->to('tabla');
        }

        
 // -------------------------------------------------------------------
        public function ver_tabla(){
            $uno = ModeloP5::
            where('estado_Visual','1')->get();
            return view('PW/ver_tabla')
            ->with('usuario',$uno);
        }
// -------------------------------------------------------------------
    }
    ?>