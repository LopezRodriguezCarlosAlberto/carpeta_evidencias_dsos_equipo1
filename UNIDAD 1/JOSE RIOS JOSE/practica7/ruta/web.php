<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('hola','PW_controller\Mi_controlador@index');
//08/01/2019  
Route::get('login','PW_controller\Mi_controlador@login');
//08/02/2019  
Route::get('ver_datos','PW_controller\Mi_controlador@ver_datos');
//12/02/2019
Route::get('ver_datosView','PW_controller\Mi_controlador@ver_datosView');

//practica 2//14/02/2019
Route::get('practica2','PW_controller\practica2control@ver_nuevosdatos2');
Route::get('formulario','PW_controller\practica2control@verformulario');
Route::post('insertar','PW_controller\practica2control@insertardatos');

//practica 03
Route::get('formularioTienda','PW_controller\practica3@verformulario');
Route::post('insertarTienda','PW_controller\practica3@insertardatos');
Route::get('practica3','PW_controller\practica3@ver_nuevosdatos2');

//practica 04
Route::get('formularioTienda4/{id}','PW_controller\practica4@verformulario');
Route::put('actualizarTienda/{id}','PW_controller\practica4@actualizarD');


//practica 05
//PRACTICA 5 ENVIO MATERIAS-ALUMNOS
Route::get('insertar_practica5','PW_controller\practica5@ver_formulario');//FORMULARIO PARA AGREGAR UN NUEVO REGISTRO
Route::post('insertar5','PW_controller\practica5@insertar'); //BOTON DE INSERTAR
Route::get('editar_practica5/{id}','PW_controller\practica5@edit_datos'); //CONSULTAR DEACUERDO AL ID
Route::put('actualizar_datos/{id}','PW_controller\practica5@actualizar_datos'); //BOTON DE ACTUALIZAR CAMBIANDO DATOS

//practica 06 eliminar
 
Route::get('mostrar_practica6/{id}','PW_controller\practica6@mostrar_datos'); 
Route::put('eliminar_datos/{id}','PW_controller\practica6@eliminar_datos'); 

//practica 06 eliminar por bandera

//ver la tablita 
//practica 8

Route::get('tabla','PW_controller\ControladorTabla@ver_tabla');
Route::get('eliminar_bandera/{id}','PW_controller\ControladorTabla@editar_bandera');
Route::get('eliminar/{id}','PW_controller\ControladorTabla@edit_datos_eliminar');
Route::get('actualizar/{id}','PW_controller\ControladorTabla@edit_datos');
Route::put('actualizar_datostabla/{id}','PW_controller\ControladorTabla@actualizar_datos');



//practica ferreteria
Route::get('ferreteriaCrud','PW_controller\ControladorFerreteria@ver_tabla');
Route::post('insertarFerreteria','PW_controller\ControladorFerreteria@insertar'); 
Route::get('editarFerre/{id}','PW_controller\ControladorFerreteria@edit_datos');
Route::get('eliminar_banderaFerre/{id}','PW_controller\ControladorFerreteria@editar_bandera');
Route::get('eliminarFerre/{id}','PW_controller\ControladorFerreteria@edit_datos_eliminar');
Route::put('actualizar_datosFerre/{id}','PW_controller\ControladorFerreteria@actualizar_datos');


//AJAX

Route::get('lista_alumnos/{genero}','AJAX\AjaxController@listado_alumnos');
Route::get('ajax','AJAX\AjaxController@formu');

//Ejercicio 2 Materias y Semestres AJAX//
Route::get('ajaxSemestre','AJAX\AjaxController@formuSemestre');
Route::get('lista_materias/{semestre}','AJAX\AjaxController@listado_semestre');

Route::get('/', function () {
    return view('welcome');
});
