<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head> 
    <meta charset="utf-8">
    <title></title>
    <style>
        input{
         display: block;
         border: 1px solid #ccc;
        box-sizing: border-box;
        }
        form{
            padding:2px 0 10px 0;
            cursor:pointer;
            display:block;
            width:220px;
            margin: 0 auto;
            text-aling:left;
        }
        </style>
  </head>
    <body>
      {!!Form::open(array('url'=>'actualizar_datostabla/'.$uno->id,'method'=>'PUT','autocomplete'=>'off'))!!}
      
      {!!Form::label('RFC: ') !!}
        {!!Form::text('rfc',$uno->rfc)!!}
        <br>
        {!!Form::label('CURP: ') !!}
        {!!Form::text('curp',$uno->curp)!!}
        <br>
        {!!Form::label('NumControl: ') !!}
        {!!Form::text('ncontrol',$uno->ncontrol)!!}
        <br>
        {!!Form::label('Materia1: ')!!}
        {!!Form::select('materia1', array($uno->materia1=>$uno->materia1), null)!!}
        <br>
        {!!Form::label('Calificacion1: ') !!}
        {!!Form::text('calif1',$uno->calif1)!!}
        <br>
        {!!Form::label('Materia2: ')!!}
        {!!Form::select('materia2', array($uno->materia2=>$uno->materia2), null)!!}
        <br>
        {!!Form::label('Calificacion2: ') !!}
        {!!Form::text('calif2',$uno->calif2)!!}
        <br>
        {!!Form::label('Materia3: ')!!}
        {!!Form::select('materia3', array($uno->materia3=>$uno->materia3), null)!!}
        <br>
        {!!Form::label('Calificacion3: ') !!}
        {!!Form::text('calif3',$uno->calif3)!!}
        <br>
        {!!Form::label('Promedio: ') !!}
        {!!Form::text('promedio',$uno->promedio)!!}
        <br>
        {!!Form::label('Fecha: ') !!}
        {!!Form::date('fecha',$uno->fecha)!!}
        <br><br>

        {!!Form::submit('Actualizar',['rfc'=>'grabar','curp'=>'grabar',
            'ncontrol'=>'grabar','materia1'=>'grabar','materia2'=>'grabar','materia3'=>'grabar',
            'calif1'=>'grabar','calif2'=>'grabar','calif3'=>'grabar','promedio'=>'grabar','fecha'=>'grabar',
            'content'=>'<span>Registrar<span>'])!!}

        {!!Form::close()!!}

  </body>
</html>