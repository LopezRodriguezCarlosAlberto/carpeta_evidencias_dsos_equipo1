<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

//practica 06 eliminar
 
Route::get('mostrar_practica6/{id}','PW_controller\practica6@mostrar_datos'); 
Route::put('eliminar_datos/{id}','PW_controller\practica6@eliminar_datos'); 

//practica 06 eliminar por bandera

Route::get('/', function () {
    return view('welcome');
});
