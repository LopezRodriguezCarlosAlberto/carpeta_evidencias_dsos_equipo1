<?php
//PARA LA BASE DE DATOS
namespace App\Models\PW;

use Illuminate\Database\Eloquent\Model;
 
class practica2modelo extends Model
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'practica2';
    //aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    //aqui van los elementos a mostrarse en cuestion
    protected $fillable = [
        'id','nombre','aPaterno','aMaterno','edad','direc','tel'
    ];
}
?>