<?php

namespace app\Http\Controllers\Dsos_controller;

use Illuminate\Http\Request;
 
use App\Http\Controllers\Controller;

use App\Models\PW\practica2modelo; //PARA LA BASE DE DATOS

class practica2control extends Controller
	{
		//PARA LA BASE DE DATOS CON BLADE
		public function ver_nuevosdatos2()
		{
			$practica2 = practica2modelo:: //Ejemplo_modelo
				select('id','nombre','aPaterno','aMaterno','edad','direc','tel')->take(1)->first();

				return view('DSOS/practica2')->with('variable',$practica2);
		}
	}
?>