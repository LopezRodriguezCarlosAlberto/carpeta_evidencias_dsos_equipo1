<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

//practica 2
Route::get('practica2','Dsos_controller\practica2control@ver_nuevosdatos2');

Route::get('/', function () {
    return view('welcome');
});
