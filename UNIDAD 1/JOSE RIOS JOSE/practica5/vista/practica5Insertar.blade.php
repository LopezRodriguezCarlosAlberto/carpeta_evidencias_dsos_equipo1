<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>practica 5</title>
    <style>
        input{
         display: block;
         border: 1px solid #ccc;
        box-sizing: border-box;
        }
        </style>
    </head>
    <body>
        {!!Form::open(array('url'=>'insertar5','method'=>'POST','autocomplete'=>'off'))!!}
        
        {!!Form::label('RFC: ') !!}
        {!!Form::text('rfc',null)!!}
        <br>
        {!!Form::label('CURP: ') !!}
        {!!Form::text('curp',null)!!}
        <br>
        {!!Form::label('NumControl: ') !!}
        {!!Form::text('ncontrol',null)!!}
        <br>
        {!!Form::label('Materia1: ')!!}
        {!!Form::select('materia1', array('Español' => 'Español', 'Matematicas' => 'Metematicas', 'Etica' => 'Etica'), null)!!}
        <br>
        {!!Form::label('Calificacion1: ') !!}
        {!!Form::text('calif1',null)!!}
        <br>
        {!!Form::label('Materia2: ')!!}
        {!!Form::select('materia2', array('Español' => 'Español', 'Matematicas' => 'Metematicas', 'Etica' => 'Etica'), null)!!}
        <br>
        {!!Form::label('Calificacion2: ') !!}
        {!!Form::text('calif2',null)!!}
        <br>
        {!!Form::label('Materia3: ')!!}
        {!!Form::select('materia3', array('Español' => 'Español', 'Matematicas' => 'Metematicas', 'Etica' => 'Etica'), null)!!}
        <br>
        {!!Form::label('Calificacion3: ') !!}
        {!!Form::text('calif3',null)!!}
        <br>
        {!!Form::label('Promedio: ') !!}
        {!!Form::text('promedio',null)!!}
        <br>
        {!!Form::label('Fecha: ') !!}
        {!!Form::date('fecha',null)!!}
        <br><br>

        {!!Form::submit('Registrar',['rfc'=>'grabar','curp'=>'grabar',
            'ncontrol'=>'grabar','materia1'=>'grabar','materia2'=>'grabar','materia3'=>'grabar',
            'calif1'=>'grabar','calif2'=>'grabar','calif3'=>'grabar','promedio'=>'grabar','fecha'=>'grabar',
            'content'=>'<span>Registrar<span>'])!!}

        {!!Form::close()!!}
    </body>
</html>