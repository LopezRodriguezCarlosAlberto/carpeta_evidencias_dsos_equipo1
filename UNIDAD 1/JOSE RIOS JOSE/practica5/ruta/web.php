<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

//practica 05
//PRACTICA 5 ENVIO MATERIAS-ALUMNOS
Route::get('insertar_practica5','PW_controller\practica5@ver_formulario');//FORMULARIO PARA AGREGAR UN NUEVO REGISTRO
Route::post('insertar5','PW_controller\practica5@insertar'); //BOTON DE INSERTAR
Route::get('editar_practica5/{id}','PW_controller\practica5@edit_datos'); //CONSULTAR DEACUERDO AL ID
Route::put('actualizar_datos/{id}','PW_controller\practica5@actualizar_datos'); //BOTON DE ACTUALIZAR CAMBIANDO DATOS



Route::get('/', function () {
    return view('welcome');
});
