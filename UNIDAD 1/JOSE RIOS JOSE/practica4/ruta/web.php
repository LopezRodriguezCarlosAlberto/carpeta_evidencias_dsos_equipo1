<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/
//practica 04
Route::get('formularioTienda4/{id}','PW_controller\practica4@verformulario');
Route::post('actualizarTienda/{id}','PW_controller\practica4@actualizarD');

Route::get('/', function () {
    return view('welcome');
});
